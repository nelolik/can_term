#include "ui_mainwindow.h"
#include "mainwindow.h"
#include "fileoperations.cpp"
#include <QVector>
#include <QFileDialog>
#include <QDebug>
#include <QIntValidator>
#include <QValidator>
#include <QRegExp>
#include <QRegExpValidator>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    if(ui->actionSLAVNA->isChecked()) { usbInterf = SLAVNA;}
    else {usbInterf = SYSTEC;}
    CanBaudRate = ui->busSpeedCBox->currentIndex();
    can = new Slavna(this);
    systec = new Systec(CanBaudRate, this);
    writeFile = new FileOperations(this);
    lastOpenedFile = "..";
    connect(ui->actionSLAVNA, SIGNAL(changed()), this, SLOT(slotActionSlavna()));
    connect(ui->actionSYSTEC, SIGNAL(changed()), this, SLOT(slotActionSystec()));
    connect(this, SIGNAL(signalSetOutput(QString)), ui->outputArea, SLOT(append(QString)));
    connect(ui->buttonReinit, SIGNAL(clicked(bool)), this, SLOT(slotReconnect()));
    connect(ui->reinit2Btn,SIGNAL(clicked(bool)), this, SLOT(slotReconnect()));
    connect(ui->scanButton, SIGNAL(clicked(bool)), this, SLOT(slotStartScanNet()));
    connect(ui->stopScanButton, SIGNAL(clicked(bool)), this, SLOT(slotStopScanNet()));
    connect(ui->clearBtn, SIGNAL(clicked(bool)), this, SLOT(slotClearOutput()));
    connect(ui->clrOutputBtn2, SIGNAL(clicked(bool)), this, SLOT(slotClearOutput2()));
    connect(ui->clrBuffBtn, SIGNAL(clicked(bool)), this, SLOT(slotClearAndStop()));
    connect(ui->stop2Btn, SIGNAL(clicked(bool)), this, SLOT(slotClearAndStop()));
    connect(ui->startReadBtn, SIGNAL(clicked(bool)), this, SLOT(slotStartReadMemory()));
    connect(ui->busSpeedCBox, SIGNAL(currentIndexChanged(int)), this, SLOT(setBusSpeed(int)));
    connect(ui->ButtonStartWrite, SIGNAL(clicked(bool)), this, SLOT(slotWriteToCan()));
    connect(ui->startWriteInputBtn, SIGNAL(clicked(bool)), this, SLOT(slotWriteToAdress()));

    connect(can, SIGNAL(signalLogRead(uchar*, uint)), this, SLOT(slotReciveDataArray(uchar*, uint)));
    connect(can, SIGNAL(signalMessage(QString)), ui->outputArea, SLOT(append(QString)));
    connect(can, SIGNAL(signalMessage2(QString)), ui->outputArea2, SLOT(append(QString)));
    connect(can, SIGNAL(signalBoth(QString)), ui->outputArea, SLOT(append(QString)));
    connect(can, SIGNAL(signalBoth(QString)), ui->outputArea2, SLOT(append(QString)));
    connect(can, SIGNAL(signalBoth(QString)), ui->messageLabl, SLOT(setText(QString)));
    connect(can, SIGNAL(signalProgress(int)), ui->progressBar, SLOT(setValue(int)));

    connect(systec, SIGNAL(signalBoth(QString)), ui->messageLabl, SLOT(setText(QString)));
    connect(systec, SIGNAL(signalBoth(QString)), ui->outputArea, SLOT(append(QString)));
    connect(systec, SIGNAL(signalProgress(int)), ui->progressBar, SLOT(setValue(int)));
    connect(systec, SIGNAL(signalLogEnd(uchar*,uint)), this, SLOT(slotReciveDataArray(uchar*,uint)));
    connect(systec, SIGNAL(signalMainOut(QString)), ui->outputArea, SLOT(append(QString)));
    connect(systec, SIGNAL(signalMainOut2(QString)), ui->outputArea2, SLOT(append(QString)));

    can->initCan(CanBaudRate);

    QRegExp expr("(0x){0,1}[0-9a-fA-F]*");
    QRegExpValidator* regValidator = new QRegExpValidator(expr, this);
    ui->adressInput->setValidator(regValidator);
    ui->lengthInput->setValidator(regValidator);
    ui->adresWriteLineEdit->setValidator(regValidator);
}

MainWindow::~MainWindow()
{
    delete ui;
    can->exit(0);
    systec->exit(0);
}

void MainWindow::slotReciveDataArray(uchar *data, uint length)
{

    writeFile->writeGetReady(data, length);

    writeFile->start();

    ui->progressBar->setValue(0);

}

void MainWindow::slotStartLog1()
{
    if(usbInterf == SLAVNA)
    {
        can->setLogType(1);
        can->start();
    }
    else
    {
        systec->setLogType(1);
        systec->start();
    }
}
void MainWindow::slotStartLog2()
{
    if(usbInterf == SLAVNA)
    {
        can->setLogType(2);
        can->start();
    }
    else
    {
        systec->setLogType(2);
        systec->start();
    }
}

void MainWindow::slotStartReadMemory()
{
    bool success = false;
    ulong adress;
    ulong length;
    QString adr = ui->adressInput->text();
    if(adr.length() > 0)
    {
        if(adr.length() > 2 && adr.at(1) == 'x')
        {
            adress = adr.toLong(&success, 16);
        }
        else
        {
            adress = adr.toLong(&success, 10);
        }
    }
    else
    {
        adress = 0;
    }
    if(!success)
    {
        ui->outputArea->append("Адрес в десятичной или шестнадцатеричной форме");
        ui->adressInput->setFocus();
        return;
    }
    QString len = ui->lengthInput->text();
    if(len.length() > 0)
    {
        if(len.length() > 2 && len.at(1) == 'x')
        {
            length = len.toLong(&success, 16);
        }
        else
        {
            length = len.toLong(&success, 10);
        }
    }
    else
    {
        length = 0;
        success = false;
    }
    if(!success)
    {
        ui->outputArea->append("Длина в десятичной или шестнадцатеричной форме");
        ui->lengthInput->setFocus();
        return;
    }
    if(usbInterf == SLAVNA && !can->isRunning())
    {
        can->slotReadMem(adress, length);
        can->start();
    }
    else if(usbInterf == SYSTEC && !systec->isRunning())
    {
        systec->slotReadMem(adress, length);
        systec->start();
    }
}


void MainWindow::setBusSpeed(int cbx_indx)
{
    if(usbInterf == SLAVNA)
    {
        if(can->isRunning())
        {
            ui->messageLabl->setText("Подождите когда CAN освободится");
            ui->busSpeedCBox->setCurrentIndex(CanBaudRate);
            return;
        }
        CanBaudRate = cbx_indx;
        can->setSpeed(CanBaudRate);
    }
    else
    {
        if(systec->isRunning())
        {
            ui->messageLabl->setText("Подождите когда CAN освободится");
            ui->busSpeedCBox->setCurrentIndex(CanBaudRate);
            return;
        }
        CanBaudRate = cbx_indx;
        systec->setSpeed(CanBaudRate);
    }
}

void MainWindow::slotReconnect()
{
    if(usbInterf == SLAVNA) {can->slotReconnect(CanBaudRate);}
    else {systec->reInit(CanBaudRate);}
}

void MainWindow::slotClearAndStop()
{
    if(usbInterf == SLAVNA) {
        can->stopThread();
        can->clear();
    }
    else if(usbInterf == SYSTEC) {
        systec->stopThread();
        systec->clearBuffers();
    }
    ui->progressBar->setValue(0);
}

void MainWindow::slotWriteToCan()
{
    lastOpenedFile = QFileDialog::getOpenFileName(this, "Открыть файл", lastOpenedFile, "*.*");
    QFile* fileToWrite = new QFile(lastOpenedFile);
    if(usbInterf == SLAVNA)
    {
        if(!can->isRunning())
        {
            if(fileToWrite->exists())
            {
                can->writeGetReady(fileToWrite, ui->flashTypeCBox->currentIndex() + 1,
                                   ui->eepromCBox->currentIndex() + 11);
                can->start();
            }
            else
            {
                ui->messageLabl->setText("Файл не открылся или отсутствует");
                delete fileToWrite;
            }
        }
    }
    else if(usbInterf == SYSTEC)
    {
        if(fileToWrite->exists())
        {
            systec->writeGetReady(fileToWrite, ui->flashTypeCBox->currentIndex() + 1,
                               ui->eepromCBox->currentIndex() + 11);
            systec->start();
        }
        else
        {
            ui->messageLabl->setText("Файл не открылся или отсутствует");
            delete fileToWrite;
        }
    }
}

void MainWindow::slotWriteToAdress()
{
    uint adress;
    bool success;
    QString len = ui->adresWriteLineEdit->text();
    if(len.length() > 0)
    {
        if(len.length() > 2 && len.at(1) == 'x')
        {
            adress = len.toLong(&success, 16);
        }
        else
        {
            adress = len.toLong(&success, 10);
        }
    }
    else
    {
        success = false;
    }
    if(!success)
    {
        ui->outputArea->append("Адрес в десятичной или шестнадцатеричной форме");
        ui->adressInput->setFocus();
        return;
    }

    lastOpenedFile = QFileDialog::getOpenFileName(this, "Открыть файл", lastOpenedFile, "*.*");
    QFile* fileToWrite = new QFile(lastOpenedFile);
    if(usbInterf == SLAVNA)
    {
        if(!can->isRunning())
        {
            if(fileToWrite->exists())
            {
                can->writeGetReady(fileToWrite, 0, 0);
                can->writeSetAdress(adress);
                can->start();
            }
            else
            {
                ui->messageLabl->setText("Файл не открылся или отсутствует");
                delete fileToWrite;
            }
        }
    }
    else if(usbInterf == SYSTEC)
    {
        if(fileToWrite->exists())
        {
            systec->writeGetReady(fileToWrite, 0, 0);
            systec->writeSetAdress(adress);
            systec->start();
        }
        else
        {
            ui->messageLabl->setText("Файл не открылся или отсутствует");
            delete fileToWrite;
        }
    }
}

void MainWindow::slotClearOutput()
{
    ui->outputArea->clear();
    ui->progressBar->setValue(0);
}

void MainWindow::slotClearOutput2()
{
    ui->outputArea2->clear();
    ui->progressBar->setValue(0);
}

void MainWindow::slotActionSlavna()
{
    if(ui->actionSLAVNA->isChecked())
    {
        ui->actionSYSTEC->setChecked(false);
        can->slotReconnect(CanBaudRate);
        usbInterf = SLAVNA;
    }
    else
    {
        ui->actionSYSTEC->setChecked(true);
        ui->actionSLAVNA->setChecked(false);

    }
}
void MainWindow::slotActionSystec()
{
    if(ui->actionSYSTEC->isChecked())
    {
        ui->actionSLAVNA->setChecked(false);
        systec->reInit(CanBaudRate);
        usbInterf = SYSTEC;
    }
    else
    {
        ui->actionSLAVNA->setChecked(true);
        ui->actionSYSTEC->setChecked(false);
    }
}

void MainWindow::slotStartScanNet()
{
    if(usbInterf == SLAVNA)
    {
        if(!can->isRunning())
        {
            can->canMonitorStart();
        }
    }
    else if(usbInterf == SYSTEC)
    {
        if(!systec->isRunning())
        {
            systec->slotStartScanNet();
        }
    }
}

void MainWindow::slotStopScanNet()
{
    if(usbInterf == SLAVNA)
    {
        can->canMonitorStop();
    }
    else if(usbInterf == SYSTEC)
    {
        systec->slotStopScanNet();
    }
}
