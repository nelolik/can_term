#include "systec.h"
#include "modebox.h"
#include "crcdata.h"
#include "crcdata.cpp"
#include <QDebug>
#include <QTime>

Systec::Systec(uint baud, QObject* parent) :QThread(parent)
{
    switch (baud) {
    case 0:
        baudRate = USBCAN_BAUD_125kBit;
        break;
    case 1:
        baudRate =  USBCAN_BAUD_250kBit;
        break;
    case 2:
        baudRate = USBCAN_BAUD_500kBit;
        break;
    case 3:
        baudRate = USBCAN_BAUD_1MBit;
        break;
    default:
        baudRate = USBCAN_BAUD_250kBit;
        break;
    }
    deviceNbr = 255; //первый встретившийся CAN модуль
    int ret;
    ret = UcanInitHardware(&UcanHandle, deviceNbr, nullptr);
    if(ret == USBCAN_SUCCESSFUL)
    {
        ret = UcanInitCan(UcanHandle, HIBYTE(baudRate), LOBYTE(baudRate), 0xFFFFFFFF, 0);
        if(ret == USBCAN_SUCCESSFUL)
        {
            emit signalBoth("Systec подключен");
        }
        else
        {
            error(ret);
        }
    }
    else
    {
        error(ret);
    }
    op = GETLOG;
    logType = 1;
    running = true;
}

Systec::~Systec()
{
    UcanDeinitCan(UcanHandle);
    UcanDeinitHardware(UcanHandle);
}

void Systec::reInit(uint baud)
{
    switch (baud) {
    case 0:
        baudRate = USBCAN_BAUD_125kBit;
        break;
    case 1:
        baudRate =  USBCAN_BAUD_250kBit;
        break;
    case 2:
        baudRate = USBCAN_BAUD_500kBit;
        break;
    case 3:
        baudRate = USBCAN_BAUD_1MBit;
        break;
    default:
        baudRate = USBCAN_BAUD_250kBit;
        break;
    }
    UcanDeinitCan(UcanHandle);
    UcanDeinitHardware(UcanHandle);

    int ret;
    ret = UcanInitHardware(&UcanHandle, deviceNbr, nullptr);
    if(ret == USBCAN_SUCCESSFUL)
    {
        ret = UcanInitCan(UcanHandle, HIBYTE(baudRate), LOBYTE(baudRate), 0xFFFFFFFF, 0);
        if(ret == USBCAN_SUCCESSFUL)
        {
            emit signalBoth("Systec подключен");
        }
        else
        {
            error(ret);
        }
    }
    else
    {
        error(ret);
    }
}

void Systec::setSpeed(uint speed)
{
    switch (speed) {
    case 0:
        baudRate = USBCAN_BAUD_125kBit;
        break;
    case 1:
        baudRate =  USBCAN_BAUD_250kBit;
        break;
    case 2:
        baudRate = USBCAN_BAUD_500kBit;
        break;
    case 3:
        baudRate = USBCAN_BAUD_1MBit;
        break;
    default:
        baudRate = USBCAN_BAUD_250kBit;
        break;
    }
    int ret = UcanSetBaudrate(UcanHandle, HIBYTE(baudRate), LOBYTE(baudRate));
    if(ret == USBCAN_SUCCESSFUL)
    {
        qDebug() << "Set baudrate success";
    }
    else
    {
        error(ret);
    }
}

void Systec::run()
{
    switch(op)
    {
    case GETLOG:  getLogBy100Mess(); break;
    case SCANNET: readMess(); break;
    case WRITE:   writeTo(); break;
    case READMEM: readMemory(); break;
    }
}

void Systec::error(int err)
{
    switch(err)
    {
    case USBCAN_SUCCESSFUL:
        qDebug() << "USBCAN_SUCCESSFUL";
        emit signalBoth("USBCAN_SUCCESSFUL");
        break;
    case USBCAN_ERR_MAXINSTANCES:
        qDebug() << "USBCAN_ERR_MAXINSTANCES";
        emit signalBoth("USBCAN_ERR_MAXINSTANCES");
        break;
    case USBCAN_ERR_ILLHANDLE:
        qDebug() << "USBCAN_ERR_ILLHANDLE";
        emit signalBoth("USBCAN_ERR_ILLHANDLE");
        break;
    case USBCAN_ERR_RESOURCE:
        qDebug() << "USBCAN_ERR_RESOURCE";
        emit signalBoth("USBCAN_ERR_RESOURCE");
        break;
    case USBCAN_ERR_BUSY:
        qDebug() << "USBCAN_ERR_BUSY";
        emit signalBoth("USBCAN_ERR_BUSY");
        break;
    case USBCAN_ERR_IOFAILED:
        qDebug() << "USBCAN_ERR_IOFAILED";
        emit signalBoth("USBCAN_ERR_IOFAILED");
        break;
    case USBCAN_ERR_ILLPARAM:
        qDebug() << "USBCAN_ERR_ILLPARAM";
        emit signalBoth("USBCAN_ERR_ILLPARAM");
        break;
    case USBCAN_ERR_ILLHW:
        qDebug() << "USBCAN_ERR_ILLHW";
        emit signalBoth("USBCAN_ERR_ILLHW");
        break;
    case USBCAN_ERR_DISCONNECT:
        qDebug() << "USBCAN_ERR_DISCONNECT";
        emit signalBoth("USBCAN_ERR_DISCONNECT");
        break;
    case USBCAN_ERR_TIMEOUT:
        qDebug() << "USBCAN_ERR_TIMEOUT";
        emit signalBoth("USBCAN_ERR_TIMEOUT");
        break;
    case USBCAN_ERRCMD:
        qDebug() << "USBCAN_ERRCMD";
        emit signalBoth("USBCAN_ERRCMD");
        break;
    case USBCAN_ERR_CANNOTINIT:
        qDebug() << "USBCAN_ERR_CANNOTINIT";
        emit signalBoth("USBCAN_ERR_CANNOTINIT");
        break;
    case USBCAN_ERR_ILLCHANNEL:
        qDebug() << "USBCAN_ERR_ILLCHANNEL";
        emit signalBoth("USBCAN_ERR_ILLCHANNEL");
        break;
    case USBCAN_WARN_NODATA:
        qDebug() << "USBCAN_WARN_NODATA";
        emit signalBoth("USBCAN_WARN_NODATA");
        break;
    case USBCAN_WARN_SYS_RXOVERRUN:
        qDebug() << "USBCAN_WARN_SYS_RXOVERRUN";
        emit signalBoth("USBCAN_WARN_SYS_RXOVERRUN");
        break;
    case USBCAN_WARN_DLL_RXOVERRUN:
        qDebug() << "USBCAN_WARN_DLL_RXOVERRUN";
        emit signalBoth("USBCAN_WARN_DLL_RXOVERRUN");
        break;
    case USBCAN_WARN_FW_RXOVERRUN:
        qDebug() << "USBCAN_WARN_FW_RXOVERRUN";
        emit signalBoth("USBCAN_WARN_FW_RXOVERRUN");
        break;
    case USBCAN_ERR_DLL_TXFULL:
        qDebug() << "USBCAN_ERR_DLL_TXFULL";
        emit signalBoth("USBCAN_ERR_DLL_TXFULL");
        break;
    case USBCAN_WARN_FW_TXOVERRUN:
        qDebug() << "USBCAN_WARN_FW_TXOVERRUN";
        emit signalBoth("USBCAN_WARN_FW_TXOVERRUN");
        break;
    }
    stopThread();
}

void Systec::getLog(int log_Type)
{
    tCanMsgStruct InMsg[20];
    tCanMsgStruct OutMsg;
    uint receivedCRC;
    uint dataLength;
    uint data1;
    uint data3;
    uint command;

    clearBuffers();

    OutMsg.m_dwID = SEND_BOX_ID;
    OutMsg.m_bFF = USBCAN_MSG_FF_EXT;
    OutMsg.m_bDLC = 8;
    OutMsg.m_bData[4] = 0xE0;
    OutMsg.m_bData[5] = REQUEST_LOG_RECIEVE;
    OutMsg.m_bData[6] = 0x00;
    OutMsg.m_bData[7] = log_Type;
    OutMsg.m_bData[0] = 0;
    OutMsg.m_bData[1] = 0;
    OutMsg.m_bData[2] = 0;
    OutMsg.m_bData[3] = 0;

    QTime totalTimer;
    totalTimer.start();
    UCANRET ret = UcanWriteCanMsg(UcanHandle, &OutMsg);
    if(USBCAN_CHECK_TX_SUCCESS(ret))
    {
        emit signalMainOut(("Запрос на получение лога ") + QString::number(log_Type) + " отправлен.");
    }
    else
    {
        error(ret);
        if(ret == USBCAN_ERR_DLL_TXFULL)
        {
            ret = UcanWriteCanMsg(UcanHandle, &OutMsg);
            if(USBCAN_CHECK_TX_SUCCESS(ret))
            {
                emit signalMainOut("Запрос на получение лога " + QString::number(log_Type )+ " отправлен.");
            }
            else
            {
                error(ret);
                return;
            }
        }
    }
    receivedCRC = receiveCRC();

    dataLength = receiveDataLength();

    if(!Running()) { return;}
    if(!dataLength) { return;}

    OutMsg.m_dwID = SEND_BOX_ID;
    OutMsg.m_bFF = USBCAN_MSG_FF_EXT;
    OutMsg.m_bDLC = 8;
    OutMsg.m_bData[4] = 0xE0;
    OutMsg.m_bData[5] = START_LOG_RECIEVE;
    OutMsg.m_bData[6] = 0x00;
    OutMsg.m_bData[7] = log_Type;
    OutMsg.m_bData[0] = 0;
    OutMsg.m_bData[1] = 0;
    OutMsg.m_bData[2] = 0;
    OutMsg.m_bData[3] = 0;

    ret = UcanWriteCanMsg(UcanHandle, &OutMsg);
    if(USBCAN_CHECK_TX_SUCCESS(ret))
    {
        emit signalBoth(("Запрос начала получения лога ") + QString::number(log_Type) + " отправлен.");
    }
    else
    {
        error(ret);
        if(ret == USBCAN_ERR_DLL_TXFULL)
        {
            ret = UcanWriteCanMsg(UcanHandle, &OutMsg);
            if(USBCAN_CHECK_TX_SUCCESS(ret))
            {
                emit signalBoth("Запрос начала получения лога " + QString::number(log_Type )+ " отправлен.");
            }
            else
            {
                emit signalBoth("Запрос начала получения лога " + QString::number(log_Type )+ " не отправлен.");
                error(ret);
                return;
            }
        }
    }

    QTime noAnswer;
    noAnswer.start();
    QVector<tCanMsgStruct>* dataVect = new QVector<tCanMsgStruct>();
    uint oneProcent = dataLength/100;
    uint readen = 0; //Прочитанных байт
    emit signalProgress(readen/oneProcent);
    uint totalMess = 0; //Прочитанных сообщений
    uint emptyMessage = 0;
    bool receiveEnd = false;
    bool arrayEndRecieved = false;
    while(readen < dataLength || !receiveEnd)
    {
        if(!Running()) { return;}
        ret = UcanReadCanMsg(UcanHandle, InMsg);
        if(USBCAN_CHECK_VALID_RXCANMSG(ret))
        {
            uint i = 0;

            totalMess++;
            if((InMsg[i].m_dwID == RECIEVE_BOX_ID))
            {
                command = InMsg[i].m_bData[4] & 0x1F;
                command = (command << 8) + InMsg[i].m_bData[5];
                if(command == SEND_DATA_WORD)
                {
                    if(readen < dataLength)
                    {
                        dataVect->append(InMsg[i]);

                        readen++;
                        if(readen%oneProcent == 0) emit signalProgress(readen/oneProcent);

                        readen++;
                        if(readen%oneProcent == 0) emit signalProgress(readen/oneProcent);
                    }
                }
                if (command == SEND_ARRAY_END)
                {
                    data1 = (InMsg[i].m_bData[2] << 8) + InMsg[i].m_bData[3];
                    receiveEnd = true;
                    emit signalMainOut("Сообщение конца передачи лога " + QString::number(data1, 10) + ". Прочитали " + QString::number(totalMess, 10) + " Сообщений");
                    arrayEndRecieved = true;
                }else if(command == ERROR_MESSAGE)
                {

                    data1 = (InMsg[i].m_bData[6] << 8) + InMsg[i].m_bData[7];
                    data3 = (InMsg[i].m_bData[0] << 8) + InMsg[i].m_bData[1];
                    showCanError(data1);
                    switch(data3)
                    {
                    case DATA_LOG:
                        emit signalMainOut("При передаче лога данных");
                        break;
                    case TEMPERATURE_LOG:
                        emit signalMainOut("При передаче лога температур");
                        break;
                    default:
                        break;
                    }

                }
                emptyMessage = 0;
                noAnswer.restart();
            }

        }
        else
        {
            if(ret != USBCAN_WARN_NODATA)
            {
                error(ret);
                return;
            }
        }

        if(noAnswer.elapsed() > 2000) break;
        emptyMessage++;

    }

    emit signalMainOut("Конец массива данных. Log# " + QString::number(data1));
    emit signalMainOut("Длинна плученых данных: " + QString::number(readen));
    emit signalMainOut("Всего сообщений " + QString::number(totalMess));
    if(dataVect->length() != dataLength)
    {
        emit signalMainOut("Принято не верное число байт");
        return;
    }
    if(data1 == log_Type)
    {
        if(readen != dataLength)
        {
            emit signalBoth("Размер данных не совпадает с полученым при инициализации");

            return;
        }
    }

    uchar* data = new uchar[dataLength];
    QList<uchar> sortedData = sortVect(dataVect);

    for(int i = 0; i < sortedData.length(); i++)
    {
        data[i] = sortedData[i];

    }

    ushort rcrc = 0xffff;
    ushort crc = CRC::get_crc_16b(rcrc, data, readen);
    emit signalBoth("Посчитаный CRC:" + QString::number(crc, 16));

    if( receivedCRC != crc )
    {
        emit signalMainOut("CRC не равны");
    }

    emit signalMainOut("Время, потраченое на приём: " +
                    QString::number(totalTimer.elapsed()) + " мкс");
    emit signalLogEnd(data, sortedData.length());

}

void Systec::getLogBy100Mess()
{
    tCanMsgStruct OutMsg;
    uint dataLength;
    QVector<tCanMsgStruct>* dataVect = new QVector<tCanMsgStruct>();

    //Команда 10 - запрос на получение лога
    OutMsg.m_dwID = SEND_BOX_ID;
    OutMsg.m_bFF = USBCAN_MSG_FF_EXT;
    OutMsg.m_bDLC = 8;
    OutMsg.m_bData[4] = 0xE0;
    OutMsg.m_bData[5] = REQUEST_LOG_RECIEVE;
    OutMsg.m_bData[6] = 0x00;
    OutMsg.m_bData[7] = logType;
    OutMsg.m_bData[0] = 0;
    OutMsg.m_bData[1] = 0;
    OutMsg.m_bData[2] = 0;
    OutMsg.m_bData[3] = 0;

    QTime totalTimer;
    totalTimer.start();
    UCANRET ret = UcanWriteCanMsg(UcanHandle, &OutMsg);
    if(USBCAN_CHECK_TX_SUCCESS(ret))
    {
        emit signalBoth(("Запрос на получение лога ") + QString::number(logType) + " отправлен.");
    }
    else
    {
        error(ret);
        if(ret == USBCAN_ERR_DLL_TXFULL)
        {
            ret = UcanWriteCanMsg(UcanHandle, &OutMsg);
            if(USBCAN_CHECK_TX_SUCCESS(ret))
            {
                emit signalBoth("Запрос на получение лога " + QString::number(logType )+ " отправлен.");
            }
            else
            {
                error(ret);
                return;
            }
        }
    }

    dataLength = receiveDataLength();
    //Получаем сообщения
    if(get100mess(dataVect, dataLength) != 0) {return;}

    if(dataVect->length()*2 == dataLength)
    {
        emit signalMainOut("Приняли " + QString::number(dataLength, 16).toUpper() + " байт");
    }
    else
    {
        emit signalMainOut("Ошибка! приняли " + QString::number(dataVect->length()*2, 16).toUpper() + " байт. Должно быть "
                        + QString::number(dataLength, 16).toUpper() + " байт");
    }
    uchar* data = new uchar[dataLength];
    QList<uchar> sortedData = sortVect(dataVect);

    for(int i = 0; i < sortedData.length(); i++)
    {
        data[i] = sortedData[i];

    }
    emit signalLogEnd(data, sortedData.length());
    delete dataVect;
}

int Systec::get100mess(QVector<tCanMsgStruct>* dataVect, uint dataLength)
{
    tCanMsgStruct InMsg;
    tCanMsgStruct OutMsg;
    QVector<tCanMsgStruct> onePackVect(0x100);
    uint oneProcent = dataLength/2/100;
    uint readen = 0; //Прочитанных байт
    uint totalMess = 0; //Прочитанных сообщений
    emit signalProgress(readen/oneProcent);
    bool receiveEnd = false;
    bool arrayEndRecieved = false;
    BYTE ret;
    uint receivedCRC;
    uint quantityOfPackages = 0;
    uint packageLength = 0;
    uint startPosition = 0;
    uint command;
    uint data1, data3, lastPackageLength;
    if(dataLength > 0)
    {
        quantityOfPackages = dataLength / 2 / 0x100 + 1; //Пакеты по 0х100 сообщений по 2 байта
    }
    else
    {
        return -1;
    }
    lastPackageLength = dataLength - (quantityOfPackages - 1) * 0x200;
    if(lastPackageLength%2) { lastPackageLength = lastPackageLength/2 + 1;}
    else { lastPackageLength /=2;}
    while(quantityOfPackages > 0)
    {
        if(!Running()) { return -1;}
        packageLength = quantityOfPackages > 1 ? 0x100 : lastPackageLength;
        if(packageLength == 0 && quantityOfPackages == 1) return 0;
        onePackVect.clear();
        startPosition = dataVect->length();
        OutMsg.m_dwID = SEND_BOX_ID;
        OutMsg.m_bFF = USBCAN_MSG_FF_EXT;
        OutMsg.m_bDLC = 8;
        OutMsg.m_bData[4] = 0xE0;
        OutMsg.m_bData[5] = START_LOG_RECIEVE;
        OutMsg.m_bData[6] = 0x00;
        OutMsg.m_bData[7] = logType;
        OutMsg.m_bData[0] = 0;
        OutMsg.m_bData[1] = 0;
        OutMsg.m_bData[2] = 0;
        OutMsg.m_bData[3] = 0;

        QTime noAnswer;
        noAnswer.start();
        ret = UcanWriteCanMsg(UcanHandle, &OutMsg);
        if(!USBCAN_CHECK_TX_SUCCESS(ret))
        {
            emit signalMainOut(("Запрос начала получения лога ") + QString::number(logType) + " не отправлен.");
            return -1;
        }
        receivedCRC = receiveCRC();
        if(receivedCRC == 0)
        {
            emit signalMainOut("Ошибка контрольной суммы (=0)");
            return -1;
        }
        readen = 0;
        receiveEnd = false;
        while((readen < packageLength || !receiveEnd))
        {
            ret = UcanReadCanMsg(UcanHandle, &InMsg);
            if(USBCAN_CHECK_VALID_RXCANMSG(ret))
            {
                if((InMsg.m_dwID == RECIEVE_BOX_ID))
                {
                    command = InMsg.m_bData[4] & 0x1F;
                    command = (command << 8) + InMsg.m_bData[5];

                    if(command == SEND_DATA_WORD)
                    {
                        if(readen < packageLength)
                        {

                            onePackVect.append(InMsg);
                            readen++;
                            totalMess++;
                            if(totalMess%oneProcent == 0) emit signalProgress(totalMess/oneProcent);
                            totalMess++;
                            if(totalMess%oneProcent == 0) emit signalProgress(totalMess/oneProcent);

                        }

                    }
                    if (command == SEND_ARRAY_END)
                    {
                        data1 = (InMsg.m_bData[2] << 8) + InMsg.m_bData[3];
                        receiveEnd = true;
                        arrayEndRecieved = true;
                    }else if(command == ERROR_MESSAGE)
                    {

                        data1 = (InMsg.m_bData[6] << 8) + InMsg.m_bData[7];
                        data3 = (InMsg.m_bData[0] << 8) + InMsg.m_bData[1];
                        showCanError(data1);
                        switch(data3)
                        {
                        case DATA_LOG:
                            emit signalBoth("При передаче лога данных");
                            break;
                        case TEMPERATURE_LOG:
                            emit signalBoth("При передаче лога температур");
                            break;
                        default:
                            break;
                        }

                    }
                    noAnswer.restart();

                }

            }
            else
            {
                if(ret != USBCAN_WARN_NODATA)
                {
                    error(ret);
                    return -1;
                }
            }
            if(!Running()) { return -1;}
            if(noAnswer.elapsed() > 5000) { return -1;}
        }
        if(onePackVect.length()!= packageLength)
        {
            emit signalMainOut("Принято не верное число байт");
            qDebug() << "length="<< dataVect->length();
            qDebug() << "startPosition=" << startPosition;
            return -1;
        }
        if(readen > packageLength) readen = packageLength;
        QList<uchar> sortedData = sortVect(&onePackVect);
        uchar* data = new uchar[sortedData.length()];

        for(int i = 0; i < sortedData.length(); i++)
        {
            data[i] = sortedData[i];

        }
        ushort rcrc = 0xffff;
        ushort crc = CRC::get_crc_16(rcrc, data, sortedData.length());
        emit signalBoth("Посчитаный CRC=" + QString::number(crc, 16).toUpper());

        delete[] data;
        if( receivedCRC == crc )
        {
            emit signalBoth("CRC равны");
        }
        else
        {
            emit signalBoth("CRC не равны");
        }
        dataVect->append(onePackVect);
        quantityOfPackages--;
        qDebug() << "quantity = " << quantityOfPackages;

    }
    return 0;
}

uint Systec::receiveCRC()
{
    tCanMsgStruct InMsg;
    UCANRET ret;
    bool wait = true;
    uint receivedCRC;
    uint command;
    QTime breakTimer;
    breakTimer.start();
    while(wait)
    {
        if(!Running()) { return 0;}
        ret = UcanReadCanMsg(UcanHandle, &InMsg);
        if(USBCAN_CHECK_VALID_RXCANMSG(ret))
        {
            if(InMsg.m_dwID == RECIEVE_BOX_ID)
            {
                command = InMsg.m_bData[4] & 0x1F;
                command = (command << 8) + InMsg.m_bData[5];
                if(command == SEND_CRC)
                {
                    receivedCRC = (InMsg.m_bData[6] << 24) + (InMsg.m_bData[7] << 16) +
                            (InMsg.m_bData[0] << 8) + (InMsg.m_bData[1]);
                    emit signalMainOut("Полученый CRC= " + QString::number(receivedCRC, 16).toUpper());
                    wait = false;
                }
            }
        }
        else
        {
            if(ret != USBCAN_WARN_NODATA)
            {
                error(ret);
                return 0;
            }
        }
        if(breakTimer.elapsed() > 20000)
        {
            receivedCRC = 0;
            break;
        }
    }
    return receivedCRC;

}

uint Systec::receiveDataLength()
{
    tCanMsgStruct InMsg;
    UCANRET ret;
    bool wait = true;
    uint dataLength;
    uint command;

    QTime breakTimer;
    breakTimer.start();
    while(wait)
    {
        if(!Running()) {return 0;}
        ret = UcanReadCanMsg(UcanHandle, &InMsg);
        if(USBCAN_CHECK_VALID_RXCANMSG(ret))
        {
            if(InMsg.m_dwID == RECIEVE_BOX_ID)
            {
                command = InMsg.m_bData[4] & 0x1F;
                command = (command << 8) + InMsg.m_bData[5];
                if(command == SEND_DATA_LENGTH)
                {
                    dataLength = (InMsg.m_bData[6] << 24) + (InMsg.m_bData[7] << 16) +
                            (InMsg.m_bData[0] << 8) + (InMsg.m_bData[1]);
                    emit signalBoth("Полученая длина данных = " + QString::number(dataLength, 16));
                    wait = false;
                }
            }
        }
        else
        {
            if(ret != USBCAN_WARN_NODATA)
            {
                error(ret);
                return 0;
            }
        }
        if(breakTimer.elapsed() > 2000)
        {
            dataLength = 0;
            break;
        }
    }
    return dataLength;
}


void Systec::setLogType(int log_Type)
{
    logType = log_Type;
    op = GETLOG;
    running = true;
}


QList<uchar> Systec::sortVect(QVector<tCanMsgStruct> * dataV)
{
    QMap<uint, uchar> sortMap;
    uint key;
    for(int i = 0; i < dataV->size(); i++)
    {
        key = (dataV->at(i).m_bData[6] << 24) + (dataV->at(i).m_bData[7] << 16) +
                (dataV->at(i).m_bData[0] << 8) + (dataV->at(i).m_bData[1]);
        if(!sortMap.contains(key+1)) sortMap[key+1] = dataV->at(i).m_bData[2];
        if(!sortMap.contains(key)) sortMap[key] = dataV->at(i).m_bData[3];

    }
    return sortMap.values();

}

QList<uint> Systec::sortGetKeys(QVector<tCanMsgStruct> * dataV)
{
    QMap<uint, ushort> sortMap;
    uint key;
    for(int i = 0; i < dataV->length(); i++)
    {
        key = (dataV->at(i).m_bData[6] << 24) + (dataV->at(i).m_bData[7] << 16) +
                (dataV->at(i).m_bData[0] << 8) + (dataV->at(i).m_bData[1]);
        if(!sortMap.contains(key)) sortMap[key] = (dataV->at(i).m_bData[2] << 8) + dataV->at(i).m_bData[3];
    }
    return sortMap.keys();
}

void Systec::showCanError(uint errNbr)
{
    switch (errNbr) {
    case ERROR_NO:
        emit signalMainOut("Нет ошибок");
        emit signalMainOut2("Нет ошибок");
        break;
    case ERROR_CRC_:
        emit signalMainOut("Ошибка CRC");
        emit signalMainOut2("Ошибка CRC");
        break;
    case ERROR_NOT_ENOUTH_DATA:
        emit signalMainOut("Недостаточно данных");
        emit signalMainOut2("Недостаточно данных");
        break;
    case ERROR_DATA_OVERLOAD:
        emit signalMainOut("Переполнение данных");
        emit signalMainOut2("Переполнение данных");
        break;
    case ERROR_FLASH:
        emit signalMainOut("Ошибка прошивки");
        emit signalMainOut2("Ошибка прошивки");
        break;
    case ERROR_INIT:
        emit signalMainOut("Ошибка инциализации");
        emit signalMainOut2("Ошибка инциализации");
        break;
    default:
        break;
    }
}

void Systec::readMess()
{
    tCanMsgStruct InMsg;
    UCANRET ret;
    //read = true;

    while(Running())
    {
        ret = UcanReadCanMsg(UcanHandle, &InMsg);
        if(USBCAN_CHECK_VALID_RXCANMSG(ret))
        {
            QString str = "";
            str += "ID: " + QString::number(InMsg.m_dwID,16) + "Data: " + QString::number(InMsg.m_bData[4],16) +
                    " " + QString::number(InMsg.m_bData[5],16) + " " + QString::number(InMsg.m_bData[6],16) +
                    " " + QString::number(InMsg.m_bData[7], 16) + " " + QString::number(InMsg.m_bData[0],16) +
                    " " + QString::number(InMsg.m_bData[1],16) + " " + QString::number(InMsg.m_bData[2],16) +
                    " " + QString::number(InMsg.m_bData[3],16);
            emit signalMainOut(str);
        }
        else
        {
            if(ret != USBCAN_WARN_NODATA) { error(ret);}
            else {emit signalMainOut("IG: ");}
        }
    }
}

void Systec::slotStartScanNet()
{
    op = SCANNET;
    running = true;
    start();
}

void Systec::slotStopScanNet()
{
    mutex.lock();
    running = false;
    mutex.unlock();
}

void Systec::writeTo()
{
    QByteArray data;
    if(soursefile->open(QIODevice::ReadOnly))
    {
        data = soursefile->readAll();
        soursefile->close();
        delete soursefile;
        qDebug() << "Файл прочитан";
    }
    else
    {
        emit signalMainOut2("Файл не открылся");
        return;
    }
    ulong dataLength = data.length();
    uchar* udata = new uchar[dataLength];
    uint maxval = static_cast<uint>(-1);
    if(dataLength > maxval)
    {
        emit signalMainOut2("Размер файла слишком большой");
        qDebug() << maxval;
        return;
    }
    for(ulong i = 0; i < dataLength; i++)
    {
        udata[i] = data.at(i);
    }
    ushort crc_mask = 0xFFFF;
    ushort crc = CRC::get_crc_16(crc_mask, udata, dataLength);
    tCanMsgStruct InMsg;
    tCanMsgStruct OutMsg;
    UCANRET ret;

    clearBuffers();
    if(!Running()) { return;}

    QThread::msleep(150); //Ждём когда в цикле дойдёт до обработки нашего сообщения

    //Посылантся команда 2 для подготовки контроллера к приёму данных
    OutMsg.m_dwID = SEND_BOX_ID;
    OutMsg.m_bFF = USBCAN_MSG_FF_EXT;
    OutMsg.m_bDLC = 8;
    OutMsg.m_bData[4] = 0xE0;
    OutMsg.m_bData[5] = SEND_DATA_ADRESS;
    OutMsg.m_bData[6] = static_cast<uchar>((writeAdress >> 24)&0xFF);
    OutMsg.m_bData[7] = static_cast<uchar>((writeAdress >> 16)&0xFF);
    OutMsg.m_bData[0] = static_cast<uchar>((writeAdress >> 8) & 0xFF);
    OutMsg.m_bData[1] = static_cast<uchar>(writeAdress & 0xFF);
    OutMsg.m_bData[2] = 0;
    OutMsg.m_bData[3] = dataType;
    ret = UcanWriteCanMsg(UcanHandle, &OutMsg);
    if(USBCAN_CHECK_TX_SUCCESS(ret))
    {
        //emit signalMainOut2("Команда инициализации отправлена");
    }
    else
    {
        error(ret);
        return;

    }
    QThread::msleep(150); //Ждём когда в цикле дойдёт до обработки нашего сообщения

    //Отправляется длина файла
    OutMsg.m_dwID = SEND_BOX_ID;
    OutMsg.m_bFF = USBCAN_MSG_FF_EXT;
    OutMsg.m_bDLC = 8;
    OutMsg.m_bData[4] = 0xE0;
    OutMsg.m_bData[5] = SEND_DATA_LENGTH;
    OutMsg.m_bData[6] = static_cast<uchar>((dataLength >> 24)&0xFF);
    OutMsg.m_bData[7] = static_cast<uchar>((dataLength >> 16)&0xFF);
    OutMsg.m_bData[0] = static_cast<uchar>((dataLength >> 8)&0xFF);
    OutMsg.m_bData[1] = static_cast<uchar>(dataLength&0xFF);
    OutMsg.m_bData[2] = 0;
    OutMsg.m_bData[3] = dataType;
    ret = UcanWriteCanMsg(UcanHandle, &OutMsg);
    if(USBCAN_CHECK_TX_SUCCESS(ret))
    {
        //emit signalMainOut2("Длина файла отправлена");
    }
    else
    {
        error(ret);
        return;

    }
    QThread::msleep(150); //Ждём когда в цикле дойдёт до обработки нашего сообщения

    //Отправляем CRC
    OutMsg.m_dwID = SEND_BOX_ID;
    OutMsg.m_bFF = USBCAN_MSG_FF_EXT;
    OutMsg.m_bDLC = 8;
    OutMsg.m_bData[4] = 0xE0;
    OutMsg.m_bData[5] = SEND_CRC;
    OutMsg.m_bData[6] = 0x00;
    OutMsg.m_bData[7] = 0;
    OutMsg.m_bData[0] = static_cast<uchar>((crc >> 8)&0xFF);
    OutMsg.m_bData[1] = static_cast<uchar>(crc&0xFF);
    OutMsg.m_bData[2] = 0;
    OutMsg.m_bData[3] = dataType;
    ret = UcanWriteCanMsg(UcanHandle, &OutMsg);
    if(USBCAN_CHECK_TX_SUCCESS(ret))
    {
        //emit signalMainOut2("CRC отправлена");
    }
    else
    {
        error(ret);
        return;

    }
    if(!Running()) { return;}
    if(dataType == 0) emit signalMainOut2("Запись в контроллер началась");
    else if(dataType == 1) emit signalMainOut2("Передача данных в контроллер началась");
    else if(dataType == 2) emit signalMainOut2("Передача данных в Xilinx началась");
    else if(dataType == 3) emit signalMainOut2("Передача данных началась");
    else emit signalMainOut2("Передача данных в EEPROM началась");
    QThread::msleep(150); //Ждём когда в цикле дойдёт до обработки нашего сообщения
    uint sent = 0;
    uint procent = dataLength / 100;
    procent = procent > 0 ? procent : 1;
    emit signalProgress(0);
    //Отправляем данные
    while(sent < dataLength)
    {
        if(!Running()) { return;}

        OutMsg.m_dwID = SEND_BOX_ID;
        OutMsg.m_bFF = USBCAN_MSG_FF_EXT;
        OutMsg.m_bDLC = 8;
        OutMsg.m_bData[4] = 0xE0;
        OutMsg.m_bData[5] = SEND_DATA_WORD;
        OutMsg.m_bData[6] = (sent >> 24) & 0xFF;
        OutMsg.m_bData[7] = (sent >> 16) & 0xFF;
        OutMsg.m_bData[0] = (sent >> 8) & 0xFF;
        OutMsg.m_bData[1] = sent & 0xFF;
        OutMsg.m_bData[2] = udata[sent++];
        OutMsg.m_bData[3] = udata[sent++];

        if(sent % procent == 0) { emit signalProgress(sent/procent);}
        ret = UcanWriteCanMsg(UcanHandle, &OutMsg);
        if(!USBCAN_CHECK_TX_SUCCESS(ret))
        {
            error(ret);
        }
        QThread::usleep(150);
    }
    qDebug() << "После отправки данных. Отправлено " << sent << "байт";
    delete[] udata;

    //Отправляем команду окончания передачи данных
    OutMsg.m_bData[4] = 0xE0;
    OutMsg.m_bData[5] = SEND_ARRAY_END;
    OutMsg.m_bData[6] = 0x00;
    OutMsg.m_bData[7] = dataType;
    OutMsg.m_bData[0] = 0;
    OutMsg.m_bData[1] = 0;
    OutMsg.m_bData[2] = 0;
    OutMsg.m_bData[3] = dataType;
    ret = UcanWriteCanMsg(UcanHandle, &OutMsg);
    if(USBCAN_CHECK_TX_SUCCESS(ret))
    {
        emit signalMainOut2("Сообщение окончания передачи отправлено");
    }
    else
    {
        error(ret);
    }

    bool sucsess = false;
    QTime timer;
    timer.start();
    while(true)
    {
        if(!Running()) { return;}
        ret = UcanReadCanMsg(UcanHandle, &InMsg);
        if(USBCAN_CHECK_VALID_RXCANMSG(ret))
        {
            uint command = ((InMsg.m_bData[4] & 0x1F) << 8) + (InMsg.m_bData[5] & 0xFF);
            if (command == ERROR_MESSAGE) {
                uint errorNmbr = ((InMsg.m_bData[6] & 0xFF) << 8) + (InMsg.m_bData[7] & 0xFF);
                if(errorNmbr == 1)
                {
                    sucsess = true;
                    emit signalMainOut2("Данные в контроллер передались без ошибок");
                    break;
                }
                else
                {
                    showCanError(errorNmbr);
                    //return;
                    sucsess = false;
                    break;
                }
            }
        }
        else
        {
            if(ret != USBCAN_WARN_NODATA)
            {
                error(ret);
                return;
            }
        }

        if(timer.elapsed() > 5000)
        {
            qDebug() << "Ждём ответа";
            timer.restart();
        }
    }
    if(sucsess && dataType)
    {
        //Посылается команда начала прошивки
        OutMsg.m_bData[4] = 0xE0;
        OutMsg.m_bData[5] = START_WRITE;
        OutMsg.m_bData[6] = 0x00;
        OutMsg.m_bData[7] = 0;
        OutMsg.m_bData[0] = 0;
        OutMsg.m_bData[1] = 0;
        OutMsg.m_bData[2] = 0;
        OutMsg.m_bData[3] = dataType;
        ret = UcanWriteCanMsg(UcanHandle, &OutMsg);
        if(USBCAN_CHECK_TX_SUCCESS(ret))
        {
            emit signalMainOut2("Команда "
                            "начала прошивки отправлена");
        }
        else
        {
            error(ret);
        }

        while(true)
        {
            if(!Running()) { return;}
            ret = UcanReadCanMsg(UcanHandle, &InMsg);
            if(USBCAN_CHECK_VALID_RXCANMSG(ret))
            {
                if(InMsg.m_dwID == RECIEVE_BOX_ID)
                {
                    uint command = ((InMsg.m_bData[4] & 0x1F) << 8) + (InMsg.m_bData[5] & 0xFF);
                    if (command == ERROR_MESSAGE) {
                        uint errorNmbr = ((InMsg.m_bData[6] & 0xFF) << 8) + (InMsg.m_bData[7] & 0xFF);
                        if(errorNmbr == 1)
                        {
                            sucsess = true;
                            emit signalMainOut2("Запись во Flash прошла успешно");
                            break;
                        }
                        else
                        {
                            showCanError(errorNmbr);
                            break;
                        }
                    }
                }
            }
            else
            {
                if(ret != USBCAN_WARN_NODATA)
                {
                    error(ret);
                    return;
                }
            }

        }
    }
    else {return;}
}

void Systec::writeGetReady(QFile *file, uint dType, uint eeprom)
{
    soursefile = file;
    op = Operation::WRITE;
    if(dType < 3)  { dataType = dType;}
    else { dataType = eeprom;}
    running = true;
}

void Systec::writeSetAdress(uint adr)
{
    writeAdress = adr;
}

void Systec::readMemory()
{
    tCanMsgStruct InMsg;
    tCanMsgStruct OutMsg;
    uint receivedCRC;
    uint dataLength;
    uint data1;
    uint data3;
    uint command;
    bool wait = true;
    QVector<tCanMsgStruct>* dataVect = new QVector<tCanMsgStruct>();

    //Отправка адреса
    OutMsg.m_dwID = SEND_BOX_ID;
    OutMsg.m_bFF = USBCAN_MSG_FF_EXT;
    OutMsg.m_bDLC = 8;
    OutMsg.m_bData[4] = 0xE0;
    OutMsg.m_bData[5] = SEND_READ_ADRESS;
    OutMsg.m_bData[6] = (readAdress >> 24) & 0xFF;
    OutMsg.m_bData[7] = (readAdress >> 16) & 0xFF;
    OutMsg.m_bData[0] = (readAdress >> 8) & 0xFF;
    OutMsg.m_bData[1] = (readAdress & 0xFF);
    OutMsg.m_bData[2] = 0;
    OutMsg.m_bData[3] = 0;

    QTime totalTimer;
    totalTimer.start();
    UCANRET ret = UcanWriteCanMsg(UcanHandle, &OutMsg);
    if(USBCAN_CHECK_TX_SUCCESS(ret))
    {
        //emit signalMainOut("Адрес для чтения отправлен отправлен.");
    }
    else
    {
        error(ret);
        if(ret == USBCAN_ERR_DLL_TXFULL)
        {
            ret = UcanWriteCanMsg(UcanHandle, &OutMsg);
            if(USBCAN_CHECK_TX_SUCCESS(ret))
            {
                //emit signalMainOut("Адрес для чтения отправлен отправлен.");
            }
            else
            {
                error(ret);
                return;
            }
        }
    }
    //Получение овета от контроллера, что адрес получен.
    QTime breakTimer;
    breakTimer.start();
    while(wait)
    {
        if(!Running()) { return;}
        ret = UcanReadCanMsg(UcanHandle, &InMsg);
        if(USBCAN_CHECK_VALID_RXCANMSG(ret))
        {
            if(InMsg.m_dwID == RECIEVE_BOX_ID)
            {
                command = InMsg.m_bData[4] & 0x1F;
                command = (command << 8) + InMsg.m_bData[5];
                if(command == SEND_ARRAY_END)
                {
                    int tmp = (InMsg.m_bData[2] << 8) + (InMsg.m_bData[3]);
                    if(tmp == READ_MEMORY)
                    {
                        wait = false;
                        //emit signalMainOut("Получение адреса подтверждено");
                    }
                }
                else if(command == ERROR_MESSAGE)
                {
                    uint errNo = (InMsg.m_bData[6] << 8) + (InMsg.m_bData[7]);
                    showCanError(errNo);
                    return;
                }
            }
        }
        else
        {
            if(ret != USBCAN_WARN_NODATA)
            {
                error(ret);
                return;
            }
        }
        if(breakTimer.elapsed() > 30000)
        {
            receivedCRC = 0;
            break;
        }
    }
    //Отправляем длину получаемых данных
    OutMsg.m_bData[4] = 0xE0;
    OutMsg.m_bData[5] = SEND_READ_LENGTH;
    OutMsg.m_bData[6] = (readLength >> 24) & 0xFF;
    OutMsg.m_bData[7] = (readLength >> 16) & 0xFF;
    OutMsg.m_bData[0] = (readLength >> 8) & 0xFF;
    OutMsg.m_bData[1] = (readLength & 0xFF);
    OutMsg.m_bData[2] = 0;
    OutMsg.m_bData[3] = 0;

    ret = UcanWriteCanMsg(UcanHandle, &OutMsg);
    if(USBCAN_CHECK_TX_SUCCESS(ret))
    {
        //emit signalMainOut("Длина данных отправлена.");
    }
    else
    {
        error(ret);
        return;
    }

    //Получаем от контроллера контрольную сумму
    receivedCRC = receiveCRC();
    if(receivedCRC == 0)
    {
        emit signalMainOut("Не корректная контрольная сумма");
        return;
    }
    //Получаем от контроллера длину получаемых данных
    dataLength = receiveDataLength();
    if(dataLength == 0 || dataLength != readLength)
    {
        emit signalMainOut("Не корректная длина данных");
        return;
    }

    if(!Running()) { return;}
    //Послать сообщение начала передачи
    OutMsg.m_bData[4] = 0xE0;
    OutMsg.m_bData[5] = START_LOG_RECIEVE;
    OutMsg.m_bData[6] = 0;
    OutMsg.m_bData[7] = 0;
    OutMsg.m_bData[0] = 0;
    OutMsg.m_bData[1] = 0;
    OutMsg.m_bData[2] = 0;
    OutMsg.m_bData[3] = 0;

    ret = UcanWriteCanMsg(UcanHandle, &OutMsg);
    if(USBCAN_CHECK_TX_SUCCESS(ret))
    {
        emit signalMainOut("Чтение данных началось.");
    }
    else
    {
        error(ret);
    }


    QTime noAnswer;
    noAnswer.start();
    dataVect->reserve(dataLength/2);
    uint oneProcent = dataLength/100 != 0 ? dataLength/100: 1;
    uint readen = 0; //Прочитанных байт
    uint totalMess = 0;
    emit signalProgress(readen/oneProcent);
    bool receiveEnd = false;
    bool arrayEndRecieved = false;
    while((readen < dataLength || !receiveEnd))
    {
        if(!Running()) { return;}
        ret = UcanReadCanMsg(UcanHandle, &InMsg);
        if(USBCAN_CHECK_VALID_RXCANMSG(ret))
        {

            totalMess++;
            //qDebug() << InMsg[i].m_dwID;
            if((InMsg.m_dwID == RECIEVE_BOX_ID))
            {
                command = InMsg.m_bData[4] & 0x1F;
                command = (command << 8) + InMsg.m_bData[5];

                if(command == SEND_DATA_WORD)
                {
                    if(readen < dataLength)
                    {
                        dataVect->append(InMsg);

                        readen++;
                        if(readen%oneProcent == 0) emit signalProgress(readen/oneProcent);

                        readen++;
                        if(readen%oneProcent == 0) emit signalProgress(readen/oneProcent);

                    }
                }
                if (command == SEND_ARRAY_END)
                {
                    data1 = (InMsg.m_bData[2] << 8) + InMsg.m_bData[3];
                    receiveEnd = true;
                    emit signalMainOut("Сообщение конца передачи лога " + QString::number(data1, 10) + ". Прочитали " + QString::number(totalMess, 10) + " Сообщений");
                    arrayEndRecieved = true;
                }else if(command == ERROR_MESSAGE)
                {

                    data1 = (InMsg.m_bData[6] << 8) + InMsg.m_bData[7];
                    data3 = (InMsg.m_bData[0] << 8) + InMsg.m_bData[1];
                    showCanError(data1);
                    switch(data3)
                    {
                    case DATA_LOG:
                        emit signalMainOut("При передаче лога данных");
                        break;
                    case TEMPERATURE_LOG:
                        emit signalMainOut("При передаче лога температур");
                        break;
                    default:
                        break;
                    }

                }
                noAnswer.restart();
            }
        }
        else
        {
            if(ret != USBCAN_WARN_NODATA)
            {
                error(ret);
                return;
            }
        }

        if(noAnswer.elapsed() > 2000) break;
    }
    emit signalMainOut("Конец массива данных.");
    emit signalMainOut("Длинна плученых данных: " + QString::number(readen));
    emit signalMainOut("Всего сообщений " + QString::number(totalMess));
    if(dataVect->length() != dataLength)
    {
        emit signalMainOut("Принято не верное число байт");
        return;
    }


    uchar* data = new uchar[dataLength];
    QList<uchar> sortedData = sortVect(dataVect);

    for(int i = 0; i < sortedData.length(); i++)
    {
        data[i] = sortedData[i];

    }

    ushort rcrc = 0xffff;
    ushort crc = CRC::get_crc_16b(rcrc, data, static_cast<ulong>(readen));

    if( receivedCRC == crc )
    {
        //emit signalMainOut("CRC равны равны");
    }
    else
    {
        emit signalMainOut("CRC не равны");
    }

    emit signalMainOut("Время, потраченое на приём: " +
                    QString::number(totalTimer.elapsed()) + " мкс");
    emit signalLogEnd(data, sortedData.length());
}

void Systec::slotReadMem(uint adr, uint length)
{
    readAdress = adr;
    readLength = length;
    op = READMEM;
    running = true;
}

void Systec::stopThread()
{
    mutex.lock();
    running = false;
    mutex.unlock();
}

bool Systec::Running()
{
    mutex.lock();
    bool tmp = running;
    mutex.unlock();
    return tmp;
}

void Systec::clearBuffers()
{
    tCanMsgStruct OutMsg;

    OutMsg.m_dwID = SEND_BOX_ID;
    OutMsg.m_bFF = USBCAN_MSG_FF_EXT;
    OutMsg.m_bDLC = 8;
    OutMsg.m_bData[4] = 0xE0;
    OutMsg.m_bData[5] = CLEAR_ALL;
    OutMsg.m_bData[6] = 0;
    OutMsg.m_bData[7] = 0;
    OutMsg.m_bData[0] = 0;
    OutMsg.m_bData[1] = 0;
    OutMsg.m_bData[2] = 0;
    OutMsg.m_bData[3] = 0;


    UCANRET ret = UcanWriteCanMsg(UcanHandle, &OutMsg);
    if(USBCAN_CHECK_TX_SUCCESS(ret))
    {
        qDebug() << "Буферы очищены";
    }
    else
    {
        error(ret);
    }
}
