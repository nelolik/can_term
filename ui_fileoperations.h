/********************************************************************************
** Form generated from reading UI file 'fileoperations.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FILEOPERATIONS_H
#define UI_FILEOPERATIONS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_FileOperations
{
public:
    QVBoxLayout *verticalLayout;
    QSpacerItem *verticalSpacer_2;
    QLabel *fileNameLbl;
    QLineEdit *fileNameLineEdit;
    QSpacerItem *verticalSpacer;
    QDialogButtonBox *buttons;

    void setupUi(QDialog *FileOperations)
    {
        if (FileOperations->objectName().isEmpty())
            FileOperations->setObjectName(QStringLiteral("FileOperations"));
        FileOperations->resize(400, 215);
        verticalLayout = new QVBoxLayout(FileOperations);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_2);

        fileNameLbl = new QLabel(FileOperations);
        fileNameLbl->setObjectName(QStringLiteral("fileNameLbl"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(fileNameLbl->sizePolicy().hasHeightForWidth());
        fileNameLbl->setSizePolicy(sizePolicy);
        fileNameLbl->setTextFormat(Qt::AutoText);

        verticalLayout->addWidget(fileNameLbl);

        fileNameLineEdit = new QLineEdit(FileOperations);
        fileNameLineEdit->setObjectName(QStringLiteral("fileNameLineEdit"));

        verticalLayout->addWidget(fileNameLineEdit);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        buttons = new QDialogButtonBox(FileOperations);
        buttons->setObjectName(QStringLiteral("buttons"));
        buttons->setOrientation(Qt::Horizontal);
        buttons->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttons);


        retranslateUi(FileOperations);
        QObject::connect(fileNameLineEdit, SIGNAL(textEdited(QString)), FileOperations, SLOT(slotSetFileEdited(QString)));
        QObject::connect(fileNameLineEdit, SIGNAL(textChanged(QString)), FileOperations, SLOT(slotSetFileName(QString)));
        QObject::connect(buttons, SIGNAL(rejected()), FileOperations, SLOT(slotOkButton()));
        QObject::connect(buttons, SIGNAL(accepted()), FileOperations, SLOT(slotEscapeButton()));

        QMetaObject::connectSlotsByName(FileOperations);
    } // setupUi

    void retranslateUi(QDialog *FileOperations)
    {
        FileOperations->setWindowTitle(QApplication::translate("FileOperations", "Dialog", 0));
        fileNameLbl->setText(QApplication::translate("FileOperations", "\320\237\320\276\320\273\320\275\321\213\320\271 \320\277\321\203\321\202\321\214 \320\272 \321\204\320\260\320\271\320\273\321\203:", 0));
    } // retranslateUi

};

namespace Ui {
    class FileOperations: public Ui_FileOperations {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FILEOPERATIONS_H
