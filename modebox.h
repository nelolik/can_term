#ifndef MODEBOX
#define MODEBOX
//Команда модбаса
#define CLEAR_ALL 1
#define SEND_DATA_ADRESS 2
#define SEND_DATA_LENGTH 3
#define SEND_CRC 4
#define SEND_DATA_WORD 5
#define SEND_ARRAY_END 6
#define START_WRITE 7
#define SEND_READ_LENGTH 8
#define ERROR_MESSAGE 9
#define REQUEST_LOG_RECIEVE 10
#define SEND_READ_ADRESS 11
#define START_LOG_RECIEVE 12

//Тип данных
#define FLASH_CONTROLLER 1
#define FLASH_XILINX 2
#define FLASH_PERIFF 3

//Тип лога
#define DATA_LOG 1
#define TEMPERATURE_LOG 2
#define READ_MEMORY 3

//Тип ошибки
#define ERROR_NO 1
#define ERROR_CRC_ 2
#define ERROR_NOT_ENOUTH_DATA 3
#define ERROR_DATA_OVERLOAD 4
#define ERROR_FLASH 5
#define ERROR_INIT 6
//Номера ящиков
#define SEND_BOX_ID 0x0CEB0E1
#define RECIEVE_BOX_ID 0x0CEB0F1

#endif // MODEBOX

