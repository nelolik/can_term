#include "fileoperations.h"
#include <QFileDialog>
#include <QFile>
#include <QDebug>
#include <QDate>
#include <QTime>
#include <QTextStream>

FileOperations::FileOperations(QWidget *parent = 0): QThread(parent)
{
    dataArr = nullptr;
    arrLength = 0;
    byteArrSize = 0;
    byteArr = nullptr;
    p = parent;
    filename = "..\\";
}

void FileOperations::writeGetReady(uchar *dat, ulong length)
{
    dataArr = dat;
    QString msg = "Value of dataArr " ;
    arrLength = length;
    byteArrSize = length;
    byteArr = new char[byteArrSize];
    QDate date = QDate::currentDate();
    QTime time = QTime::currentTime();
    QString file;
    file =filename + QString::number(date.year())+"_"+QString::number(date.month())+"_"+
            QString::number(date.day())+"_"+QString::number(time.hour())+"."+
            QString::number(time.minute())+"."+QString::number(time.second());
    filename = QFileDialog::getSaveFileName(p, "Сохранить как", file, "*.bin;;*.*");
}

void FileOperations::run()
{
    qDebug() << filename;
    QFile file(filename);

    if(file.open(QIODevice::WriteOnly))
    {
        QString msg = "File open";
        qDebug() << msg;
        if(dataArr != nullptr && dataArr != 0 && arrLength > 0 && byteArr != nullptr && byteArrSize > 0)
        {
            for(uint i = 0; i < byteArrSize; i++)
            {
                byteArr[i] = static_cast<char>(dataArr[i]);

            }
            file.write(byteArr, arrLength);
            QString tmp = "Is written";
            qDebug() << tmp;
            file.close();
        }

    }

/*    QFile txt_file(filename + ".txt");
    if(txt_file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        ushort word;
        QString text;
        if(dataArr != nullptr && dataArr != 0 && arrLength > 0 && byteArr != nullptr && byteArrSize > 0)
        {
            for(uint i = 0; i < byteArrSize; i+=2)
            {
                word = ((dataArr[i] << 8) & 0xFF00) + dataArr[i+1];
                text += QString::number(word) + "; ";
            }
            QTextStream stream(&txt_file);
            stream << text;
            QString tmp = "Is written";
            qDebug() << tmp;
            file.close();
            delete[] byteArr;
            delete[] dataArr;

        }
        else
        {
            //emit signalMessage("НЕ достаточно данных для записи");
        }
    }
*/
}
