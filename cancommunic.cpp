#include "cancommunic.h"
#include "cancommunic2.cpp"
#include "cancommunicwrite.cpp"
#include "crcdata.h"
#include <iostream>
#include <QTime>
#include <QDebug>

//#define MyProj

using namespace std;

Slavna::Slavna(QObject *parent) : QThread(parent)
{

    arrayEndRecieved = false;
    soursefile = nullptr;
    op = Operation::EMPTY;
    baudRate = 0;
    running = true;
    TimeOut = 1000;

}

void Slavna::initCan(uint baudrateIdx)
{
    BOOL b;
    ULONG devicecount;
    baudRate = baudrateIdx;
    // Загрузим библиотеку. Необходимо это сделать один раз
    // до вызова любой другой функции библиотеки.
    b=SlCan_Load(NULL,NULL);
    if(b)
    {

        // Выясним, сколько устройств доступно
        devicecount = SlCan_GetDeviceCount();
        if(devicecount)
        {
            // Получим ссылку на первое устройство
            hDevice = SlCan_GetDevice(0);

            if (hDevice!=INVALID_HANDLE_VALUE)
            {
                // Откроем устройство.
                b = SlCan_DeviceOpen(hDevice);
                    if (b)

                    {
                        switch (baudRate) {
                        case 0:
                            br.BRP = SLCAN_BR_CIA_125K;
                            break;
                        case 1:
                            br.BRP = SLCAN_BR_CIA_250K;
                            break;
                        case 2:
                            br.BRP = SLCAN_BR_CIA_500K;
                            break;
                        case 3:
                            br.BRP = SLCAN_BR_CIA_1000K;
                            break;
                        default:
                            break;
                        }
                        b = SlCan_DeviceSetBitRate(hDevice,&br);
                        if(b)
                        {

                            // Установим режим приёма и передачи
                            b = SlCan_DeviceSetMode(hDevice,SLCAN_MODE_NORMAL);
                            emit signalBoth("Slavna подключен");
                        } else
                        {
                            emit signalBoth("Ошибка установки скорости");
                        }
                    }
                    else
                    {
                        emit signalBoth("Устройство не открылось");
                    }

            }
            else
            {
                emit signalBoth("Ссылка на устройство не получена");
            }


        }
        else
        {
            emit signalBoth("Устройства не доступны");
        }

    }
    else
    {
        emit signalBoth("Библиотека SlCan не загружена");
    }

}

Slavna::~Slavna()
{
    SlCan_Free(false);
}


void Slavna::getLog(int log_type)
{
    SLCAN_MESSAGE InMsg[200];
    ULONG ReadCount = 0;

    uint recievedCRC = 0;
    uint command = 0;
    uint data1 = 0;
    uint data2 = 0;
    uint data3 = 0;
    uint dataLength = 0;
    uint logType = log_type;
    wait = true;
    boolean recieveEnd = false;
    uint attempt = 0;
    uint emptyMessage = 0;
    QString tmp = "";
    QVector<SLCAN_MESSAGE>* dataVect = new QVector<SLCAN_MESSAGE>();

    QTime recieveTimer;
    recieveTimer.start();
    //Отправляем сообщение с запросом на получение лога данных
    requestLogReceive(logType);
    recievedCRC = recieveCRC();
    attempt = 0;
    wait = true;
    emptyMessage = 0;

    dataLength = recieveDataLength();
    if(!Running()) { return;}
    if(dataLength == 0)
    {
        emit signalMessage("Некорректная длина данных (=0)");
        return;
    }
    //Послать сообщение начала передачи данных
    startLogReceive(logType);
    wait = true;
    attempt = 0;
    emptyMessage = 0;

    /////////////////
    bool shown = false; //debug-debug
    //Читаем данные
    BOOL b;
    dataVect->reserve(dataLength/2);
    uint oneProcent = dataLength/100;
    readen = 0; //Прочитанных байт
    emit signalProgress(readen/oneProcent);
    ulong totalMess = 0; //Прочитанных сообщений
    QTime timeoutError;
    timeoutError.start();
    while(readen < dataLength || !recieveEnd)
    {
        if(!Running()) { return;}
        b = SlCan_DeviceReadMessages(hDevice, TimeOut, InMsg, 1, &ReadCount);
        if(b == 0)
        {
            emit signalMessage("Ошибка передачи данных");
            return;
        }

        for(uint i = 0; i < ReadCount; i++)
        {
            totalMess++;
            if((InMsg[i].ID == RECIEVE_BOX_ID))
            {
                command = InMsg[i].Data[4] & 0x1F;
                command = (command << 8) + InMsg[i].Data[5];

                if(command == SEND_DATA_WORD)
                {
                    if(readen < dataLength)
                    {

                        dataVect->append(InMsg[i]);
                        readen++;
                        if(readen%oneProcent == 0) emit signalProgress(readen/oneProcent);
                        readen++;
                        if(readen%oneProcent == 0) emit signalProgress(readen/oneProcent);
                    }
                    if(dataVect->length() > 192512 && !shown)
                    {
                        qDebug() << dataVect->length();
                        qDebug() << readen;
                        shown = true;
                    }
                }
                else if (command == SEND_ARRAY_END)
                {
                    data1 = (InMsg[i].Data[2] << 8) + InMsg[i].Data[3];
                    recieveEnd = true;
                    emit signalMessage("Сообщение конца передачи лога " + tmp.setNum(data1, 10) + ". Прочитали " + tmp.setNum(totalMess, 10) + " Сообщений");

                }
                else if(command == ERROR_MESSAGE)
                {

                    data1 = (InMsg[i].Data[6] << 8) + InMsg[i].Data[7];
                    data3 = (InMsg[i].Data[0] << 8) + InMsg[i].Data[1];
                    showCanError(data1);
                    switch(data3)
                    {
                    case DATA_LOG:
                        emit signalMessage("При передаче лога данных лог данных");
                        break;
                    case TEMPERATURE_LOG:
                        emit signalMessage("При передаче лога температур");
                        break;
                    default:
                        break;
                    }
                }
                timeoutError.restart();
                emptyMessage = 0;
            }
        }
        if(timeoutError.elapsed() > 10000)
        {
            emit signalMessage("Получение данных прервано. Большая задержка.");
            emit signalMessage("Получено " + QString::number(dataVect->length()) + " сообщений");
            emit signalMessage("Размер вектора " + QString::number(dataVect->size()) + " readen=" + QString::number(readen));
            qDebug() << totalMess;
            qDebug() << readen;
            qDebug() << dataVect->length();
            return;
        }
        //emptyMessage++;
        //if(emptyMessage > 20) break;
        getState();
    }
    emit signalMessage("Конец массива данных. Log# " + tmp.setNum(data1));
    emit signalMessage("Длинна полученых данных: " + tmp.setNum(dataVect->length()));
    emit signalMessage("Всего сообщений " + tmp.setNum(totalMess));
    if(data1 == logType)
    {
        emit signalMessage("Проверка типа данных пройдена");
        if(readen != dataLength)
        {
            emit signalMessage("Размер данных не совпадает с полученым при инициализации");

            //return;
        }
    }

    uint timeElapsed = recieveTimer.elapsed()/1000;
    if(timeElapsed > 60)
    {
        int secs = timeElapsed % 60;
        int mins = timeElapsed / 60;
        emit signalMessage("Время, потраченое на приём: " + QString::number(mins) +
                           " мин " + QString::number(secs) + " сек");
    }
    else
    {
        emit signalMessage("Время, потраченое на приём: " + QString::number(timeElapsed) + " сек");
    }



    uchar* data = new uchar[dataLength];
    QList<uchar> sortedData = sortVect(dataVect);

    for(int i = 0; i < sortedData.length(); i++)
    {
        data[i] = sortedData[i];

    }
/*    uint* keys = new uint[readen/2];
    QList<uint> sortedKeys = sortGetKeys(dataVect);
    for(int i = 0; i < sortedKeys.length(); i++)
    {
        keys[i] = sortedKeys[i];
    }                                      //Получаем массив порядковых номеров сообщений в отладочных целях
*/


    int rcrc = 0xffff;
    int crc = CRC::get_crc_16b(rcrc, data, readen);
    emit signalMessage("Посчитаный CRC:" + tmp.setNum(crc, 16));

    if( recievedCRC == crc )
    {
        emit signalMessage("CRC равны");
    }
    else
    {
        emit signalMessage("CRC не равны");
    }



    emit signalLogRead(data, sortedData.length());
}

bool Slavna::requestLogReceive(int log_type)
{
    //Послать запрос на получение лога
    OutMsg.Info = SLCAN_MES_INFO_EXT;
    OutMsg.ID = SEND_BOX_ID;
    OutMsg.DataCount = 8;
    OutMsg.Data[0] = 0x00;
    OutMsg.Data[1] = 0x00;
    OutMsg.Data[2] = 0x00;
    OutMsg.Data[3] = 0x00;
    OutMsg.Data[4] = 0xE0;
    OutMsg.Data[5] = REQUEST_LOG_RECIEVE;
    OutMsg.Data[6] = 0x00;
    OutMsg.Data[7] = log_type;
    BYTE writeStatus;

    BYTE b = SlCan_DeviceWriteMessages(hDevice, &OutMsg, 1,&writeStatus);
    if(b == 0)
    {
        emit signalMessage("Запрос на получение лога не отправлен");
        showErrorSlaviaMessage(writeStatus);
        return false;
    }
    return true;
}

bool Slavna::startLogReceive(int log_type)
{
    //Послать сообщение начала передачи данных
    OutMsg.Info = SLCAN_MES_INFO_EXT;
    OutMsg.ID = SEND_BOX_ID;
    OutMsg.DataCount = 8;
    OutMsg.Data[0] = 0x00;
    OutMsg.Data[1] = 0x00;
    OutMsg.Data[2] = 0x00;
    OutMsg.Data[3] = 0x00;
    OutMsg.Data[4] = 0xE0;
    OutMsg.Data[5] = START_LOG_RECIEVE;
    OutMsg.Data[6] = 0x00;
    OutMsg.Data[7] = log_type;
    BYTE writeStatus;

    BYTE b = SlCan_DeviceWriteMessages(hDevice, &OutMsg, 1,&writeStatus);
    if(b == 0)
    {
        emit signalMessage("Запрос на получение лога не отправлен");
        showErrorSlaviaMessage(writeStatus);
        return false;
    }
    return true;
}

uint Slavna::recieveCRC()
{
    ulong ReadCount;
    uint recievedCRC = 0;
    uint command;
    SLCAN_MESSAGE InMsg[20];
    BOOL b;
    QTime noAnswerTimer;
    noAnswerTimer.start();
    wait = true;
    while(wait)
    {
        if(!Running()) { return 0;}
        b = SlCan_DeviceReadMessages(hDevice, TimeOut, InMsg, 1, &ReadCount);
        if(b == 0)
        {
            signalMessage("CRC не получен. Ошибка чтения.");
            return 0;
        }

        if((InMsg[0].ID == RECIEVE_BOX_ID) && ReadCount != 0)
        {

            command = InMsg[0].Data[4] & 0x1F;
            command = (command << 8) + InMsg[0].Data[5];
            if (command == SEND_CRC)
            {
                recievedCRC = (InMsg[0].Data[6] << 24) + (InMsg[0].Data[7] << 16) +
                        (InMsg[0].Data[0] << 8) + (InMsg[0].Data[1]);

                //emit signalMessage("CRC получен " + QString::number(recievedCRC, 16));
                wait = false;
                break;
            }

        }

        if(noAnswerTimer.elapsed() > 20000)
        {
            emit signalMessage("CRC не получен. Превышен период ожидания.");
            break;
        }
    }
    return recievedCRC;
}

uint Slavna::recieveDataLength()
{
    uint dataLength = 0;
    ulong ReadCount;
    uint command;
    bool wait = true;
    SLCAN_MESSAGE InMsg[20];
    BOOL b;
    QTime noAnswerTimer;
    noAnswerTimer.start();
    //emit signalMessage("Получаем длинну лога");
    while(wait)
    {
        if(!Running()) { return 0;}
        b = SlCan_DeviceReadMessages(hDevice, TimeOut, InMsg, 1, &ReadCount);
        if(b == 0)
        {
            emit signalMessage("Размер данных не получен");
            return 0;
        }

        if((InMsg[0].ID == RECIEVE_BOX_ID) && ReadCount != 0)
        {
            command = InMsg[0].Data[4] & 0x1F;
            command = (command << 8) + InMsg[0].Data[5];
            if (command == SEND_DATA_LENGTH)
            {
                dataLength = (InMsg[0].Data[6] << 24) & 0xFF000000;
                dataLength += (InMsg[0].Data[7] <<16) & 0x00FF0000;
                dataLength += (InMsg[0].Data[0] <<8) & 0x0000FF00;
                dataLength += (InMsg[0].Data[1]) & 0x000000FF;
                emit signalMessage("Длинна лога получена " +QString::number(dataLength));
                wait = false;
                break;
            }
        }
        if(noAnswerTimer.elapsed() > 5000)
        {
            emit signalMessage("Размер данных не получен. Превышен период ожидания.");
            break;
        }
    }
    return dataLength;
}

void Slavna::scanNet()
{
    SLCAN_MESSAGE InMess[10];
    BOOL b;
    TimeOut = 100;
    ULONG ReadCount = 0;
    if(hDevice != INVALID_HANDLE_VALUE)
    {
        while(Running())
        {
            b = SlCan_DeviceReadMessages(hDevice, TimeOut,InMess, 10, &ReadCount);
            if(b)
            {

                for(uint i= 0; i < ReadCount; i++)
                {
                    emit signalMessage("In for");
                    QString str = "";
                    QString tmp = "";
                    str = "ID: " + tmp.setNum(InMess[i].ID, 16) + " Data:";
                    for(uint j = 0; j<InMess[i].DataCount; j++)
                        str += " " + tmp.setNum(InMess[i].Data[j]);
                    emit signalMessage(str);

                }
            }
            else
            {
                emit signalMessage("Ошибка чтения порта");
            }
        }
    }
}


void Slavna::GetMode(void) {
    ULONG mode;

    if (SlCan_DeviceGetMode(hDevice, &mode)) {
        switch (mode) {
        case SLCAN_MODE_NORMAL:
            emit signalMessage("normal");
            break;
        case SLCAN_MODE_CONFIG:
            emit signalMessage("config");
            break;
        case SLCAN_MODE_LOOPBACK:
            emit signalMessage("loopback");
            break;
        case SLCAN_MODE_LISTENONLY:
            emit signalMessage("listenonly");
            break;

        default:
            emit signalMessage("unknown");
            break;
        }
    }
    else
        emit signalMessage("Ошибка получения мода");
}

void Slavna::setLogType(int type)
{
    log_Type = type;
    op = Operation::READ_LOG;
    running = true;
}

void Slavna::run()
{
    switch (op) {
    case READ_LOG:
        getLogBy100Mess(log_Type);
        //getLog(log_Type);
        break;
    case READ_MEM:
        readMemory();
        break;
    case WRITE:
        writeTo();
        break;
    case SCAN_NET:
        scanNet();
        break;
    default:
        break;
    }


}



void Slavna::slotReadMem(uint adr, uint length)
{
    readAdress = adr;
    readLength = length;
    op = READ_MEM;
    running = true;
}

void Slavna::slotReconnect(uint baud)
{
    baudRate = baud;
    SlCan_Free(false);
    initCan(baudRate);
}
void Slavna::canMonitorStart()
{
    op = SCAN_NET;
    running = true;
    start();
}
void Slavna::canMonitorStop()
{
    stopThread();
}

void Slavna::stopThread()
{
    mutex.lock();
    running = false;
    mutex.unlock();
}

bool Slavna::Running()
{
    mutex.lock();
    bool tmp = running;
    mutex.unlock();
    return tmp;
}

QList<uchar> Slavna::sortVect(QVector<SLCAN_MESSAGE> * dataV)
{
    QMap<uint, uchar> sortMap;
    uint key;
    for(int i = 0; i < dataV->size(); i++)
    {
        key = (dataV->at(i).Data[6] << 24) + (dataV->at(i).Data[7] << 16) +
                (dataV->at(i).Data[0] << 8) + (dataV->at(i).Data[1]);
        if(!sortMap.contains(key)) sortMap[key] = dataV->at(i).Data[2];
        if(!sortMap.contains(key+1)) sortMap[key+1] = dataV->at(i).Data[3];
    }
    return sortMap.values();
}

QList<uint> Slavna::sortGetKeys(QVector<SLCAN_MESSAGE>* dataV)
{
    QMap<uint, ushort> sortMap;
    uint key;
    for(int i = 0; i < dataV->length(); i++)
    {
        key = (dataV->at(i).Data[6] << 24) + (dataV->at(i).Data[7] << 16) +
                (dataV->at(i).Data[0] << 8) + (dataV->at(i).Data[1]);
        if(!sortMap.contains(key)) sortMap[key] = dataV->at(i).Data[2] << 8;
        if(!sortMap.contains(key + 2)) sortMap[key + 1] = dataV->at(i).Data[3];
    }
    return sortMap.keys();
}

