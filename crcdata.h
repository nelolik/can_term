#ifndef CRCDATA
#define CRCDATA

#define MAKE_TABS 0 /* Builds tables below */
#define FAST_CRC 1 /* If fast CRC should be used */
#define ONLY_CRC16 1

class CRC
{
public:
static unsigned short crc_16_tab[];

static unsigned short get_crc_16(unsigned short crc,unsigned char *buf,unsigned int size );

static unsigned short get_crc_16b(unsigned short crc,unsigned char *buf,unsigned int size );

};

#endif // CRCDATA

