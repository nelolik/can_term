#ifndef FILEOPERATIONS_H
#define FILEOPERATIONS_H

//#include <QObject>
#include <QThread>

class FileOperations : public QThread
{

private:
    QString filename;
    uchar* dataArr;
    ulong arrLength;
    char* byteArr;
    ulong byteArrSize;
    QWidget* p;
public:
    FileOperations( QWidget *parent);
    void writeGetReady(uchar* dat, ulong length);
    void run();

};

#endif // FILEOPERATIONS_H
