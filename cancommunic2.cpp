#include "cancommunic.h"
#include "crcdata.h"
#include <iostream>
#include <QTime>
#include <QDebug>

void Slavna::getLogBy100Mess(int log_type)
{
    uint dataLength;
    QVector<SLCAN_MESSAGE>* dataVect = new QVector<SLCAN_MESSAGE>();
    if(!requestLogReceive(log_type)) { return;}
    dataLength = recieveDataLength();
    if(dataLength == 0)
    {
        emit signalMessage("Не корректная длина данных (=0)");
        return;
    }
    if(get100mess(dataVect, dataLength, log_type) == -1) {
        emit signalMessage("Ошибка при получении данных");
        return;
    }
    if(dataVect->length()*2 == dataLength)
    {
        emit signalMessage("Приняли " + QString::number(dataLength) + " байт. Чтение завершено.");
    }
    else
    {
        emit signalMessage("Ошибка! приняли " + QString::number(dataVect->length()*2) + " байт. Должно быть "
                        + QString::number(dataLength) + " байт");
    }
    uchar* data = new uchar[dataLength];
    QList<uchar> sortedData = sortVect(dataVect);

    for(int i = 0; i < sortedData.length(); i++)
    {
        data[i] = sortedData[i];

    }
    emit signalLogRead(data, sortedData.length());
    delete dataVect;
}

int Slavna::get100mess(QVector<SLCAN_MESSAGE> *dataVect, uint dataLength, int log_type)
{
    SLCAN_MESSAGE InMsg;
    QVector<SLCAN_MESSAGE> onePackVect(0x100);
    uint oneProcent = dataLength / 100;
    oneProcent = oneProcent ? oneProcent : 1;
    uint readen = 0;    //байт в пакете из 100 сообщений
    uint totalBytes = 0; //всего сообщений
    emit signalProgress(totalBytes/oneProcent);
    bool receiveEnd = false;
    BYTE b;
    uint receivedCRC, quantityOfPackages, packageLength, lastPackageLength, startPosition;
    uint command, data1, data3;
    DWORD ReadCount;
    if(dataLength > 0)
    {
        quantityOfPackages = dataLength / 2 / 0x100 + 1;
    }
    else {return -1;}
    lastPackageLength = dataLength - (quantityOfPackages - 1) * 0x200;
    if (lastPackageLength % 2) { lastPackageLength = lastPackageLength / 2 + 1;}
    else { lastPackageLength /=2;}
    QTime noAnswer;

    ////////////////////////////////
    while(quantityOfPackages > 0)
    {
        packageLength = quantityOfPackages > 1 ? 0x100 : lastPackageLength;
        if(packageLength == 0 && quantityOfPackages == 1) {return 0;}
        onePackVect.clear();
        startPosition = dataVect->length();

        if(!startLogReceive(log_type)) { return -1;}
        if((receivedCRC = recieveCRC()) == 0)
        {
            emit signalMessage("Ошибка! CRC = 0");
            return -1;
        }
        readen = 0;
        receiveEnd = false;
        noAnswer.start();
        while((readen < packageLength || !receiveEnd))
        {
            if(!Running()) { return -1;}
            b = SlCan_DeviceReadMessages(hDevice, TimeOut, &InMsg, 1, &ReadCount);
            if(b == 0)
            {
                emit signalMessage("Ошибка чтения данных");
                return -1;
            }
            if(InMsg.ID == RECIEVE_BOX_ID)
            {
                command = InMsg.Data[4] & 0x1F;
                command = (command << 8) + InMsg.Data[5];
                if(command == SEND_DATA_WORD)
                {
                    onePackVect.append(InMsg);
                    readen ++;
                    totalBytes++;
                    if(totalBytes % oneProcent == 0) emit signalProgress(totalBytes/oneProcent);
                    totalBytes++;
                    if(totalBytes % oneProcent == 0) emit signalProgress(totalBytes/oneProcent);
                }
                else if (command == SEND_ARRAY_END)
                {
                    data1 = (InMsg.Data[2] << 8) + InMsg.Data[3];
                    receiveEnd = true;
                }
                else if(command == ERROR_MESSAGE)
                {
                    data1 = (InMsg.Data[6] << 8) + InMsg.Data[7];
                    data3 = (InMsg.Data[0] << 8) + InMsg.Data[1];
                    showCanError(data1);
                    switch(data3)
                    {
                    case DATA_LOG:
                        emit signalMessage("При передаче лога данных");
                        return -1;
                    case TEMPERATURE_LOG:
                        emit signalMessage("При передаче лога температур");
                        return -1;
                    }
                }
                noAnswer.restart();
            }
            if(noAnswer.elapsed() > 3000) {
                qDebug() << "Error readen= " << readen;
                qDebug() << receiveEnd;
                return -1;}
        }
        if(onePackVect.length()!= packageLength)
        {
            emit signalMessage("Принято не верное число байт. packageLength != readen");
            return -1;
        }
        if(readen > packageLength) readen = packageLength;
        uchar* data = new uchar[readen*2];
        QList<uchar> sortedData = sortVect(&onePackVect);

        for(uint i = 0; i < readen*2; i++)
        {
            data[i] = sortedData[i];
        }
        ushort rcrc = 0xFFFF;
        ushort crc = CRC::get_crc_16b(rcrc, data, readen*2);
        delete[] data;
        if( receivedCRC == crc )
        {
           // emit signalMessage("CRC равны");
        }
        else
        {
            emit signalMessage("CRC не равны");
        }
        dataVect->append(onePackVect);
        quantityOfPackages--;
    }
    return 0;
}



void Slavna::showErrorSlaviaMessage(uchar errorNumber)
{
    switch (errorNumber) {
    case SLCAN_TX_STATUS_OK:
        emit signalBoth("Сообщение передано без ошибок");
        break;
    case SLCAN_TX_STATUS_TIMEOUT:
        emit signalBoth("Ошибка таймаута");
        break;
    case SLCAN_TX_STATUS_BUSOFF:
        emit signalBoth("Ошибка на шине");
        break;
    case SLCAN_TX_STATUS_ABORT:
        emit signalBoth("Передача прервана внешней командой");
        break;
    case SLCAN_TX_STATUS_NOT_ENA:
        emit signalBoth("передача запрещена");
        break;
    case SLCAN_TX_STATUS_ERROR_ONE_SHOT:
        emit signalBoth("Ошибка ONE SHOT");
        break;
    case SLCAN_TX_STATUS_INVALID_MODE:
        emit signalBoth("Режим не поддерживает передачу");
        break;
    case SLCAN_TX_STATUS_UNKNOWN:
        emit signalBoth("Ошибка неизвестна");
        break;
    default:
        break;

    }
}

void Slavna::showCanError(uint errNbr)
{
    switch (errNbr) {
    case ERROR_NO:
        emit signalMessage("Нет ошибок");
        break;
    case ERROR_CRC_:
        emit signalMessage("Ошибка CRC");
        emit signalMessage2("Ошибка CRC");
        break;
    case ERROR_NOT_ENOUTH_DATA:
        emit signalMessage("Недостаточно данных");
        emit signalMessage2("Недостаточно данных");
        break;
    case ERROR_DATA_OVERLOAD:
        emit signalMessage("Переполнение данных");
        emit signalMessage2("Переполнение данных");
        break;
    case ERROR_FLASH:
        emit signalMessage("Ошибка прошивки");
        emit signalMessage2("Ошибка прошивки");
        break;
    case ERROR_INIT:
        emit signalMessage("Ошибка инциализации");
        emit signalMessage2("Ошибка инциализации");
        break;
    default:
        break;
    }
}

void Slavna::setSpeed(int ind)
{
    baudRate = ind;
    BOOL b = SlCan_DeviceSetMode(hDevice, SLCAN_MODE_CONFIG);
    if(b != 0)
    {
        switch (baudRate) {
        case 0:
            br.BRP = SLCAN_BR_CIA_125K;
            break;
        case 1:
            br.BRP = SLCAN_BR_CIA_250K;
            break;
        case 2:
            br.BRP = SLCAN_BR_CIA_500K;
            break;
        case 3:
            br.BRP = SLCAN_BR_CIA_1000K;
            break;
        default:
            break;
        }
        b = SlCan_DeviceSetBitRate(hDevice, &br);
        if(b == 0) emit signalBoth("Ошибка изменения скорости шины");
    }
    else {
        emit signalBoth("Не удалось войти в режим конфигурации");
    }
    b = SlCan_DeviceSetMode(hDevice, SLCAN_MODE_NORMAL);
    if(b == 0) emit signalBoth("Контроллер не установлен в нормальный режим. Переподключите устройство.");
}

void Slavna::clear()
{
    OutMsg.Info = SLCAN_MES_INFO_EXT;
    OutMsg.ID = SEND_BOX_ID;
    OutMsg.DataCount = 8;
    OutMsg.Data[0] = 0x00;
    OutMsg.Data[1] = 0x00;
    OutMsg.Data[2] = 0x00;
    OutMsg.Data[3] = 0x00;
    OutMsg.Data[4] = 0xE0;
    OutMsg.Data[5] = CLEAR_ALL;
    OutMsg.Data[6] = 0x00;
    OutMsg.Data[7] = 0x00;

    BYTE writeStatus;

    //Отправляем сообщение очистки счётчиков приёма и передачи
    BOOL b;

    b = SlCan_DeviceWriteMessages(hDevice, &OutMsg, 1, &writeStatus);
    if(b == 0)
    {
        showErrorSlaviaMessage(writeStatus);
    }
}

void Slavna::getState()
{
    SLCAN_STATE state;
    QString mess = "";
    BOOL b;
    b = SlCan_DeviceGetState(hDevice, &state);
    switch (state.BusMode) {
    case SLCAN_BUS_STATE_ERROR_ACTIVE_WARN:
        mess.append("Ошибок больше 96\n");
        break;
    case SLCAN_BUS_STATE_ERROR_PASSIVE:
        mess.append("Ошибок больше 127\n");
        break;
    case SLCAN_BUS_STATE_BUSOFF:
        mess.append("Ошибок больше 256. Режим BUS OFF\n");
        break;
    default:
        break;
    }
    if(state.ErrCountTX > 0)
    {
        mess.append("Ошибок передачи: " + QString::number(state.ErrCountTX) + "\n");
    }
    if(state.ErrCountRX > 0)
    {
        mess.append("Ошибок приёма: " + QString::number(state.ErrCountRX) + "\n");
    }
    if(mess.length() > 1) { emit signalMessage(mess);}
}

void Slavna::canPurge()
{
    SlCan_DevicePurge(hDevice, SLCAN_PURGE_TX_CLEAR);
    SlCan_DevicePurge(hDevice, SLCAN_PURGE_RX_CLEAR);
}

void Slavna::readMemory()
{
    BYTE writeStatus;
    BOOL b;
    SLCAN_MESSAGE InMsg[200];
    ULONG ReadCount = 0;
    uint command = 0;
    uint dataLength = 0;
    QVector<SLCAN_MESSAGE>* dataVect = new QVector<SLCAN_MESSAGE>();

    QTime recieveTimer;
    recieveTimer.start();
    OutMsg.Info = SLCAN_MES_INFO_EXT;
    OutMsg.ID = SEND_BOX_ID;
    OutMsg.DataCount = 8;
    OutMsg.Data[0] = (readAdress >> 8) & 0xFF;
    OutMsg.Data[1] = (readAdress & 0xFF);
    OutMsg.Data[2] = 0x00;
    OutMsg.Data[3] = 0x00;
    OutMsg.Data[4] = 0xE0;
    OutMsg.Data[5] = SEND_READ_ADRESS;
    OutMsg.Data[6] = (readAdress >> 24) & 0xFF;
    OutMsg.Data[7] = (readAdress >> 16) & 0xFF;
    TimeOut = 10;

    b = SlCan_DeviceWriteMessages(hDevice, &OutMsg, 1, &writeStatus);
    if(b == 0)
    {
        emit signalMessage("Адрес данных не отправлен");
        showErrorSlaviaMessage(writeStatus);
        return;
    }
    QTime noAnswerTimer;
    noAnswerTimer.start();
    wait = true;
    while(wait)
    {
        if(!Running()) { return;}
        b = SlCan_DeviceReadMessages(hDevice, TimeOut, InMsg, 1, &ReadCount);
        if(b == 0)
        {
            emit signalMessage("Ошибка передачи данных");
            return;
        }
        command = InMsg[0].Data[4] & 0x1F;
        command = (command << 8) + InMsg[0].Data[5];
        if (command == SEND_ARRAY_END)
        {
            int tmp = (InMsg[0].Data[2] << 8) + InMsg[0].Data[3];
            if(tmp == READ_MEMORY)
            {
                wait = false;
                emit signalMessage("Получение адреса подтверждено.");
            }
        }
        else if(command == ERROR_MESSAGE)
        {
            uint errNo = (InMsg[0].Data[6] << 8) + InMsg[0].Data[7];
            showCanError(errNo);
            return;
        }
        if(noAnswerTimer.elapsed() > 5000)
        {
            emit signalMessage("Подтверждение получения адреса не получено. Превышен период ожидания.");
            break;
        }
    }
    qDebug() << "Before length sending";
    OutMsg.Info = SLCAN_MES_INFO_EXT;
    OutMsg.ID = SEND_BOX_ID;
    OutMsg.DataCount = 8;
    OutMsg.Data[0] = (readLength >> 8) & 0xFF;
    OutMsg.Data[1] = (readLength & 0xFF);
    OutMsg.Data[2] = 0x00;
    OutMsg.Data[3] = 0x00;
    OutMsg.Data[4] = 0xE0;
    OutMsg.Data[5] = SEND_READ_LENGTH;
    OutMsg.Data[6] = (readLength >> 24) & 0xFF;
    OutMsg.Data[7] = (readLength >> 16) & 0xFF;;
    TimeOut = 10;

    b = SlCan_DeviceWriteMessages(hDevice, &OutMsg, 1, &writeStatus);
    if(b == 0)
    {
        emit signalMessage("Адрес данных не отправлен");
        showErrorSlaviaMessage(writeStatus);
        return;
    }

    dataLength = recieveDataLength();
    if(dataLength == 0 || dataLength != readLength)
    {
        emit signalMessage("Не корректная длина данных");
        return;
    }
    if(get100mess(dataVect, readLength, 0) == -1)
    {
        emit signalMessage("Ошибка при получении данных.");
        return;
    }
    emit signalMessage("Данные получены. " + QString::number(dataVect->length()*2) + " байт");

    QList<uchar> sortedData = sortVect(dataVect);
    uchar* data = new uchar[sortedData.length()];
    for(int i = 0; i < sortedData.length(); i++)
    {
        data[i] = sortedData[i];

    }
    uint timeElapsed = recieveTimer.elapsed()/1000;
    if(timeElapsed > 60)
    {
        int secs = timeElapsed % 60;
        int mins = timeElapsed / 60;
        emit signalMessage("Время, потраченое на приём: " + QString::number(mins) +
                           " мин " + QString::number(secs) + " сек");
    }
    else
    {
        emit signalMessage("Время, потраченое на приём: " + QString::number(timeElapsed) + " сек");
    }
    emit signalLogRead(data, sortedData.length());
}

