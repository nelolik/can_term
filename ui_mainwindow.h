/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionSLAVNA;
    QAction *actionSYSTEC;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout_2;
    QTabWidget *tabWidget;
    QWidget *tab;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout;
    QPushButton *log1Button;
    QPushButton *log2Button;
    QFrame *line;
    QLabel *adressLbl;
    QLineEdit *adressInput;
    QLabel *lengthLabl;
    QLineEdit *lengthInput;
    QPushButton *startReadBtn;
    QFrame *line_2;
    QPushButton *scanButton;
    QPushButton *stopScanButton;
    QSpacerItem *verticalSpacer;
    QFrame *line_3;
    QPushButton *clearBtn;
    QPushButton *clrBuffBtn;
    QPushButton *buttonReinit;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QGridLayout *gridLayout_2;
    QTextEdit *outputArea;
    QWidget *tab_2;
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout_3;
    QLabel *flashTypeLbl;
    QComboBox *flashTypeCBox;
    QLabel *adressEEPROMLbl;
    QComboBox *eepromCBox;
    QPushButton *ButtonStartWrite;
    QFrame *line_4;
    QLabel *adresWriteLbl;
    QLineEdit *adresWriteLineEdit;
    QLabel *lengthWriteLbl;
    QPushButton *startWriteInputBtn;
    QSpacerItem *verticalSpacer_2;
    QFrame *line_5;
    QPushButton *clrOutputBtn2;
    QPushButton *stop2Btn;
    QPushButton *reinit2Btn;
    QVBoxLayout *verticalLayout_4;
    QTextEdit *outputArea2;
    QFrame *frame;
    QHBoxLayout *horizontalLayout_2;
    QGroupBox *speedGrBox;
    QVBoxLayout *verticalLayout_6;
    QComboBox *busSpeedCBox;
    QSpacerItem *verticalSpacer_3;
    QSpacerItem *horizontalSpacer;
    QLabel *messageLabl;
    QProgressBar *progressBar;
    QMenuBar *menuBar;
    QMenu *menuTerm;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(656, 682);
        QIcon icon;
        icon.addFile(QStringLiteral(":/new/imagis/set_logo.jpg"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        MainWindow->setWindowOpacity(1);
        actionSLAVNA = new QAction(MainWindow);
        actionSLAVNA->setObjectName(QStringLiteral("actionSLAVNA"));
        actionSLAVNA->setCheckable(true);
        actionSLAVNA->setChecked(true);
        actionSYSTEC = new QAction(MainWindow);
        actionSYSTEC->setObjectName(QStringLiteral("actionSYSTEC"));
        actionSYSTEC->setCheckable(true);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout_2 = new QVBoxLayout(centralWidget);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(tabWidget->sizePolicy().hasHeightForWidth());
        tabWidget->setSizePolicy(sizePolicy);
        tabWidget->setIconSize(QSize(16, 16));
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        horizontalLayout = new QHBoxLayout(tab);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setSizeConstraint(QLayout::SetFixedSize);
        log1Button = new QPushButton(tab);
        log1Button->setObjectName(QStringLiteral("log1Button"));
        log1Button->setMaximumSize(QSize(110, 30));

        verticalLayout->addWidget(log1Button);

        log2Button = new QPushButton(tab);
        log2Button->setObjectName(QStringLiteral("log2Button"));
        log2Button->setMaximumSize(QSize(110, 30));

        verticalLayout->addWidget(log2Button);

        line = new QFrame(tab);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line);

        adressLbl = new QLabel(tab);
        adressLbl->setObjectName(QStringLiteral("adressLbl"));
        adressLbl->setMaximumSize(QSize(100, 30));

        verticalLayout->addWidget(adressLbl);

        adressInput = new QLineEdit(tab);
        adressInput->setObjectName(QStringLiteral("adressInput"));
        adressInput->setMaximumSize(QSize(110, 30));
        adressInput->setToolTipDuration(-1);
        adressInput->setMaxLength(10);

        verticalLayout->addWidget(adressInput);

        lengthLabl = new QLabel(tab);
        lengthLabl->setObjectName(QStringLiteral("lengthLabl"));
        lengthLabl->setMaximumSize(QSize(100, 30));

        verticalLayout->addWidget(lengthLabl);

        lengthInput = new QLineEdit(tab);
        lengthInput->setObjectName(QStringLiteral("lengthInput"));
        lengthInput->setMaximumSize(QSize(110, 30));
        lengthInput->setMaxLength(10);

        verticalLayout->addWidget(lengthInput);

        startReadBtn = new QPushButton(tab);
        startReadBtn->setObjectName(QStringLiteral("startReadBtn"));
        startReadBtn->setMaximumSize(QSize(100, 30));

        verticalLayout->addWidget(startReadBtn);

        line_2 = new QFrame(tab);
        line_2->setObjectName(QStringLiteral("line_2"));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line_2);

        scanButton = new QPushButton(tab);
        scanButton->setObjectName(QStringLiteral("scanButton"));
        scanButton->setMaximumSize(QSize(110, 30));

        verticalLayout->addWidget(scanButton);

        stopScanButton = new QPushButton(tab);
        stopScanButton->setObjectName(QStringLiteral("stopScanButton"));
        stopScanButton->setMaximumSize(QSize(110, 30));

        verticalLayout->addWidget(stopScanButton);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        line_3 = new QFrame(tab);
        line_3->setObjectName(QStringLiteral("line_3"));
        line_3->setFrameShape(QFrame::HLine);
        line_3->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line_3);

        clearBtn = new QPushButton(tab);
        clearBtn->setObjectName(QStringLiteral("clearBtn"));
        clearBtn->setMaximumSize(QSize(110, 30));

        verticalLayout->addWidget(clearBtn);

        clrBuffBtn = new QPushButton(tab);
        clrBuffBtn->setObjectName(QStringLiteral("clrBuffBtn"));
        clrBuffBtn->setMaximumSize(QSize(110, 30));

        verticalLayout->addWidget(clrBuffBtn);

        buttonReinit = new QPushButton(tab);
        buttonReinit->setObjectName(QStringLiteral("buttonReinit"));
        buttonReinit->setMaximumSize(QSize(110, 30));

        verticalLayout->addWidget(buttonReinit);


        horizontalLayout->addLayout(verticalLayout);

        scrollArea = new QScrollArea(tab);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QStringLiteral("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 303, 425));
        gridLayout_2 = new QGridLayout(scrollAreaWidgetContents);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        outputArea = new QTextEdit(scrollAreaWidgetContents);
        outputArea->setObjectName(QStringLiteral("outputArea"));
        outputArea->setMinimumSize(QSize(0, 0));
        outputArea->setMaximumSize(QSize(500, 16777215));
        outputArea->setReadOnly(false);

        gridLayout_2->addWidget(outputArea, 0, 0, 1, 1);

        scrollArea->setWidget(scrollAreaWidgetContents);

        horizontalLayout->addWidget(scrollArea);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        horizontalLayout_3 = new QHBoxLayout(tab_2);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setSizeConstraint(QLayout::SetMinimumSize);
        flashTypeLbl = new QLabel(tab_2);
        flashTypeLbl->setObjectName(QStringLiteral("flashTypeLbl"));
        flashTypeLbl->setMaximumSize(QSize(110, 30));

        verticalLayout_3->addWidget(flashTypeLbl);

        flashTypeCBox = new QComboBox(tab_2);
        flashTypeCBox->setObjectName(QStringLiteral("flashTypeCBox"));
        flashTypeCBox->setMaximumSize(QSize(110, 30));

        verticalLayout_3->addWidget(flashTypeCBox);

        adressEEPROMLbl = new QLabel(tab_2);
        adressEEPROMLbl->setObjectName(QStringLiteral("adressEEPROMLbl"));
        adressEEPROMLbl->setMaximumSize(QSize(110, 30));

        verticalLayout_3->addWidget(adressEEPROMLbl);

        eepromCBox = new QComboBox(tab_2);
        eepromCBox->setObjectName(QStringLiteral("eepromCBox"));
        eepromCBox->setMaximumSize(QSize(110, 30));

        verticalLayout_3->addWidget(eepromCBox);

        ButtonStartWrite = new QPushButton(tab_2);
        ButtonStartWrite->setObjectName(QStringLiteral("ButtonStartWrite"));
        ButtonStartWrite->setMaximumSize(QSize(100, 30));

        verticalLayout_3->addWidget(ButtonStartWrite);

        line_4 = new QFrame(tab_2);
        line_4->setObjectName(QStringLiteral("line_4"));
        line_4->setFrameShape(QFrame::HLine);
        line_4->setFrameShadow(QFrame::Sunken);

        verticalLayout_3->addWidget(line_4);

        adresWriteLbl = new QLabel(tab_2);
        adresWriteLbl->setObjectName(QStringLiteral("adresWriteLbl"));

        verticalLayout_3->addWidget(adresWriteLbl);

        adresWriteLineEdit = new QLineEdit(tab_2);
        adresWriteLineEdit->setObjectName(QStringLiteral("adresWriteLineEdit"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(adresWriteLineEdit->sizePolicy().hasHeightForWidth());
        adresWriteLineEdit->setSizePolicy(sizePolicy1);
        adresWriteLineEdit->setLayoutDirection(Qt::LeftToRight);
        adresWriteLineEdit->setMaxLength(10);

        verticalLayout_3->addWidget(adresWriteLineEdit);

        lengthWriteLbl = new QLabel(tab_2);
        lengthWriteLbl->setObjectName(QStringLiteral("lengthWriteLbl"));

        verticalLayout_3->addWidget(lengthWriteLbl);

        startWriteInputBtn = new QPushButton(tab_2);
        startWriteInputBtn->setObjectName(QStringLiteral("startWriteInputBtn"));
        startWriteInputBtn->setMaximumSize(QSize(100, 16777215));

        verticalLayout_3->addWidget(startWriteInputBtn);

        verticalSpacer_2 = new QSpacerItem(110, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_2);

        line_5 = new QFrame(tab_2);
        line_5->setObjectName(QStringLiteral("line_5"));
        line_5->setFrameShape(QFrame::HLine);
        line_5->setFrameShadow(QFrame::Sunken);

        verticalLayout_3->addWidget(line_5);

        clrOutputBtn2 = new QPushButton(tab_2);
        clrOutputBtn2->setObjectName(QStringLiteral("clrOutputBtn2"));
        clrOutputBtn2->setMaximumSize(QSize(110, 16777215));

        verticalLayout_3->addWidget(clrOutputBtn2);

        stop2Btn = new QPushButton(tab_2);
        stop2Btn->setObjectName(QStringLiteral("stop2Btn"));
        stop2Btn->setMaximumSize(QSize(110, 30));

        verticalLayout_3->addWidget(stop2Btn);

        reinit2Btn = new QPushButton(tab_2);
        reinit2Btn->setObjectName(QStringLiteral("reinit2Btn"));
        reinit2Btn->setMaximumSize(QSize(110, 30));

        verticalLayout_3->addWidget(reinit2Btn);


        horizontalLayout_3->addLayout(verticalLayout_3);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        outputArea2 = new QTextEdit(tab_2);
        outputArea2->setObjectName(QStringLiteral("outputArea2"));

        verticalLayout_4->addWidget(outputArea2);


        horizontalLayout_3->addLayout(verticalLayout_4);

        tabWidget->addTab(tab_2, QString());

        verticalLayout_2->addWidget(tabWidget);

        frame = new QFrame(centralWidget);
        frame->setObjectName(QStringLiteral("frame"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::MinimumExpanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(frame->sizePolicy().hasHeightForWidth());
        frame->setSizePolicy(sizePolicy2);
        frame->setMinimumSize(QSize(300, 0));
        frame->setMaximumSize(QSize(16777215, 100));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Plain);
        horizontalLayout_2 = new QHBoxLayout(frame);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        speedGrBox = new QGroupBox(frame);
        speedGrBox->setObjectName(QStringLiteral("speedGrBox"));
        speedGrBox->setMinimumSize(QSize(110, 30));
        verticalLayout_6 = new QVBoxLayout(speedGrBox);
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setContentsMargins(11, 11, 11, 11);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        busSpeedCBox = new QComboBox(speedGrBox);
        busSpeedCBox->setObjectName(QStringLiteral("busSpeedCBox"));
        busSpeedCBox->setEditable(false);
        busSpeedCBox->setInsertPolicy(QComboBox::InsertAtBottom);

        verticalLayout_6->addWidget(busSpeedCBox);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_6->addItem(verticalSpacer_3);


        horizontalLayout_2->addWidget(speedGrBox);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        messageLabl = new QLabel(frame);
        messageLabl->setObjectName(QStringLiteral("messageLabl"));
        QSizePolicy sizePolicy3(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(messageLabl->sizePolicy().hasHeightForWidth());
        messageLabl->setSizePolicy(sizePolicy3);
        messageLabl->setMinimumSize(QSize(300, 60));

        horizontalLayout_2->addWidget(messageLabl);


        verticalLayout_2->addWidget(frame);

        progressBar = new QProgressBar(centralWidget);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setEnabled(true);
        progressBar->setMaximumSize(QSize(16777215, 15));
        progressBar->setValue(0);

        verticalLayout_2->addWidget(progressBar);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 656, 22));
        menuTerm = new QMenu(menuBar);
        menuTerm->setObjectName(QStringLiteral("menuTerm"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuTerm->menuAction());
        menuTerm->addAction(actionSLAVNA);
        menuTerm->addAction(actionSYSTEC);

        retranslateUi(MainWindow);
        QObject::connect(log1Button, SIGNAL(clicked()), MainWindow, SLOT(slotStartLog1()));
        QObject::connect(log2Button, SIGNAL(clicked()), MainWindow, SLOT(slotStartLog2()));

        tabWidget->setCurrentIndex(0);
        busSpeedCBox->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Terminal", 0));
        actionSLAVNA->setText(QApplication::translate("MainWindow", "SLAVNA", 0));
        actionSYSTEC->setText(QApplication::translate("MainWindow", "SYSTEC", 0));
        log1Button->setText(QApplication::translate("MainWindow", "\320\237\320\276\320\273\321\203\321\207\320\270\321\202\321\214 \320\273\320\276\320\2631", 0));
        log2Button->setText(QApplication::translate("MainWindow", "\320\237\320\276\320\273\321\203\321\207\320\270\321\202\321\214 \320\273\320\276\320\2632", 0));
        adressLbl->setText(QApplication::translate("MainWindow", "\320\220\320\264\321\200\320\265\321\201", 0));
#ifndef QT_NO_TOOLTIP
        adressInput->setToolTip(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 \320\260\320\264\321\200\320\265\321\201 \321\201 \320\272\320\276\321\202\320\276\321\200\320\276\320\263\320\276 \320\275\320\260\321\207\320\270\320\275\320\260\321\202\321\214 \321\207\320\270\321\202\320\260\321\202\321\214. 0x - HEX", "\320\220\320\264\321\200\320\265\321\201 \320\277\320\260\320\274\321\217\321\202\320\270 \320\262 \321\210\320\265\321\201\321\202\320\275\320\260\320\264\321\206\320\260\321\202\320\265\321\200\320\270\321\207\320\275\320\276\320\271 \321\204\320\276\321\200\320\274\320\265"));
#endif // QT_NO_TOOLTIP
#ifndef QT_NO_STATUSTIP
        adressInput->setStatusTip(QString());
#endif // QT_NO_STATUSTIP
        adressInput->setText(QApplication::translate("MainWindow", "0x", 0));
        lengthLabl->setText(QApplication::translate("MainWindow", "\320\224\320\273\320\270\320\275\320\260", 0));
#ifndef QT_NO_TOOLTIP
        lengthInput->setToolTip(QApplication::translate("MainWindow", "\320\222\320\262\320\265\320\264\320\270\321\202\320\265 \321\200\320\260\320\267\320\274\320\265\321\200 \321\207\320\270\321\202\320\260\320\265\320\274\320\276\320\271 \320\277\320\260\320\274\321\217\321\202\320\270. 0x - HEX", 0));
#endif // QT_NO_TOOLTIP
        lengthInput->setText(QApplication::translate("MainWindow", "0x", 0));
        startReadBtn->setText(QApplication::translate("MainWindow", "\320\237\321\200\320\276\321\207\320\270\321\202\320\260\321\202\321\214", 0));
#ifndef QT_NO_TOOLTIP
        scanButton->setToolTip(QApplication::translate("MainWindow", "\320\234\320\276\320\275\320\270\321\202\320\276\321\200\320\270\320\275\320\263 CAN \321\201\320\265\321\202\320\270", 0));
#endif // QT_NO_TOOLTIP
        scanButton->setText(QApplication::translate("MainWindow", "\320\241\320\272\320\260\320\275\320\270\321\200\320\276\320\262\320\260\321\202\321\214", 0));
        stopScanButton->setText(QApplication::translate("MainWindow", "\320\236\321\201\321\202\320\260\320\275\320\276\320\262\320\270\321\202\321\214", 0));
        clearBtn->setText(QApplication::translate("MainWindow", "\320\236\321\207\320\270\321\201\321\202\320\270\321\202\321\214 \320\262\321\213\320\262\320\276\320\264", 0));
        clrBuffBtn->setText(QApplication::translate("MainWindow", "\320\236\321\201\321\202\320\260\320\275\320\276\320\262\320\270\321\202\321\214", 0));
        buttonReinit->setText(QApplication::translate("MainWindow", "\320\237\320\265\321\200\320\265\320\277\320\276\320\264\320\272\320\273\321\216\321\207\320\270\321\202\321\214\321\201\321\217", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("MainWindow", "\320\247\320\270\321\202\320\260\321\202\321\214", 0));
        flashTypeLbl->setText(QApplication::translate("MainWindow", "\320\242\320\270\320\277 \320\277\321\200\320\276\321\210\320\270\320\262\320\272\320\270", 0));
        flashTypeCBox->clear();
        flashTypeCBox->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "\320\232\320\276\320\275\321\202\321\200\320\276\320\273\320\273\320\265\321\200", 0)
         << QApplication::translate("MainWindow", "Xilinx", 0)
         << QApplication::translate("MainWindow", "EEPROM", 0)
        );
        adressEEPROMLbl->setText(QApplication::translate("MainWindow", "\320\220\320\264\321\200\320\265\321\201 \320\277\320\273\320\260\321\202\321\213 EEPROM", 0));
        eepromCBox->clear();
        eepromCBox->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "11", 0)
         << QApplication::translate("MainWindow", "12", 0)
         << QApplication::translate("MainWindow", "13", 0)
         << QApplication::translate("MainWindow", "14", 0)
         << QApplication::translate("MainWindow", "15", 0)
        );
        ButtonStartWrite->setText(QApplication::translate("MainWindow", "\320\237\321\200\320\276\321\210\320\270\321\202\321\214", 0));
        adresWriteLbl->setText(QApplication::translate("MainWindow", "\320\220\320\264\321\200\320\265\321\201", 0));
        adresWriteLineEdit->setText(QApplication::translate("MainWindow", "0x", 0));
        lengthWriteLbl->setText(QApplication::translate("MainWindow", "\320\224\320\273\320\270\320\275\320\260", 0));
        startWriteInputBtn->setText(QApplication::translate("MainWindow", "\320\227\320\260\320\277\320\270\321\201\320\260\321\202\321\214", 0));
        clrOutputBtn2->setText(QApplication::translate("MainWindow", "\320\236\321\207\320\270\321\201\321\202\320\270\321\202\321\214", 0));
        stop2Btn->setText(QApplication::translate("MainWindow", "\320\236\321\201\321\202\320\260\320\275\320\276\320\262\320\270\321\202\321\214", 0));
        reinit2Btn->setText(QApplication::translate("MainWindow", "\320\237\320\265\321\200\320\265\320\277\320\276\320\264\320\272\320\273\321\216\321\207\320\270\321\202\321\214\321\201\321\217", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("MainWindow", "\320\227\320\260\320\277\320\270\321\201\320\260\321\202\321\214", 0));
        speedGrBox->setTitle(QApplication::translate("MainWindow", "\320\241\320\272\320\276\321\200\320\276\321\201\321\202\321\214 \321\210\320\270\320\275\321\213", 0));
        busSpeedCBox->clear();
        busSpeedCBox->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "125 KB/s", 0)
         << QApplication::translate("MainWindow", "250 KB/s", 0)
         << QApplication::translate("MainWindow", "500 KB/s", 0)
         << QApplication::translate("MainWindow", "1000 KB/s", 0)
        );
        messageLabl->setText(QApplication::translate("MainWindow", "messages", 0));
        menuTerm->setTitle(QApplication::translate("MainWindow", "USB \320\270\320\275\321\202\320\265\321\200\321\204\320\265\320\271\321\201", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
