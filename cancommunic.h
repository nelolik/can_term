#include <QThread>
#include "inc/slcan.h"
#include "modebox.h"
#include <QVector>
#include <QMap>
#include <QFile>
#include <QMutex>


#ifndef CANCOMMUNIC_H
#define CANCOMMUNIC_H

#define SEND_BOX_ID 0x0CEB0E1
#define RECIEVE_BOX_ID 0x0CEB0F1

//65655
//37848
class Slavna : public QThread
{
    Q_OBJECT

private:
    HSLCAN hDevice;         //Ссылка на устройство.
    SLCAN_BITRATE br;       //Структура, сохраняющая настройки, отвечающие за тайминги в сети
    SLCAN_MESSAGE OutMsg;   //Структура, сохраняющая сообщение
    PSLCAN_MESSAGE pOutMsg; //Указатель на структуру сообщения
    uint baudRate;          //Переменная хранит индекс сорости сети для взаимодействия графического
                            //интерфейса и программы
    int TimeOut;            //Время ожидания прихода заданного числа сообщений
    bool arrayEndRecieved;  //Флаг приёма сообщения окончания передачи
    volatile bool running;  //Показывает состояния потока. Используется что бы гуманно остановить.
    QMutex mutex;           //Мьютекс для running
    int log_Type;           //Тип получаемого лога
    uint readen;            //Счётчик прочитанных сообщений
    boolean wait;           //Флаг для создания и выхода из циклов
    QFile* soursefile;      //Файл, из которого читаются данные
    uint readAdress, readLength;    //Сохраняет переданные от MainWindow адрес и длину
                                    //читаемых данных
    uint writeAdress;               //Сохраняет адрес по которому записывать в контроллере
    uint dataType;                  //Индекс типа передаваемых данных
    uint eepromNo;                  //Номер переферийного устройства, в которое нужно записать прошивку

    enum Operation {READ_LOG, READ_MEM, WRITE, SCAN_NET, EMPTY}; //Определяет выполняемое действие
    Operation op;

    uint getDataLength(SLCAN_MESSAGE mess);             //Функиция отправляет запрос и получает длину получемых данных
    QString getErrorType(SLCAN_MESSAGE mess);           //Получает и возвращает номер ошибки от контроллера
    QList<uchar> sortVect(QVector<SLCAN_MESSAGE>*);     //Сортирует полученный массив структур и возвращает
                                                        //массив байт в виде списка
    QList<uint> sortGetKeys(QVector<SLCAN_MESSAGE>*);   //Возвращает отсортированный массив адресов (в целях отладки)
    void showErrorSlaviaMessage(uchar errorNumber);     //Получает и выводит ощибки от USB-CAN конвертера
    void showCanError(uint errNbr);                     //Выводит на экран ошибку от контроллера
    void getLog(int log_type);              //Функция для получения всего объёма лога одни блоком
    void getLogBy100Mess(int log_type);     //Функция для получения лога пакетами по 0х100 сообщений
    int get100mess(QVector<SLCAN_MESSAGE>* dataVect, uint dataLength, int log_type); //Занимается непосредственно
                                            //приёмом сообщений и анализом правильности принятых данных
    bool requestLogReceive(int log_type);   //Отправляет запрос на получение лога
    bool startLogReceive(int log_type);     //Отправляет команду начала получения лога
    uint recieveCRC();                      //Получает контрольную сумму
    uint recieveDataLength();               //Получает длину передаваемых данных
    //void getLogWithRequest(int log_type);   //
    void writeTo();     //Передаёт данные в контроллер
    void getState();    //Отбражает ошибку, возвращаемую функциями передачи или отправки сообщений
    void readMemory();  //Читать область памяти, заданную в  ручную.
    bool Running();     //используется как флаг для остановки процесса
    void run();

public:     //Публичные функции для установки параметров от графического интерфейса
    Slavna(QObject* parent = 0);
    ~Slavna();
    void initCan(uint baudrateIdx); //Подключает к драйверу
    void setLogType(int type);      //Устанавливает тип лога
    void scanNet();                 //Запускает врежиме сканирования сети ??? устаревший вариант
    void GetMode();                 //Получает состояние USB-CAN конвертера
    void setSpeed(int ind);         //Утанавливает скорость соединения по переданному индексу
    void writeGetReady(QFile* file, uint dType, uint eeprom); //Устанавливает данные необходимые для передачи данных
    void writeSetAdress(uint adr);  //Устанавливает адрес, по которому писать в случае передачи по адресу
    void canPurge();                //Очищает буфферы приёма и передачи  конвертера
    void stopThread();              //Устанавливает флаг остановки потока
    void slotReadMem(uint adr, uint length);    //Устанавлевает адрес откуда читать и длину данных
    void slotReconnect(uint baud);  //Переподключает конвертер
    void canMonitorStart();         //Подгатавливает к сканироанию сети
    void canMonitorStop();          //останавливает сканарование сети
    void clear();                   //Очищает буфферы приёма и передачи контроллера

signals:
    void signalMessage(QString);    //Выводит сообщение в поле вывода чтения
    void signalMessage2(QString);   //Выводит сообщение в поле вывода записи
    void signalBoth(QString);       //Выводит соощение в поля вывода и системный Message Lable
    void signalLogRead(uchar*, uint);   //передаёт массив полученных данных и длину данных
    void signalProgress(int);           //Передаёт проценты в прогресс бар


};

#endif // CANCOMMUNIC_H
