#ifndef SYSTEC_H
#define SYSTEC_H
#include "inc/slcan.h"
#include <QThread>
#include "Systec/Include/Usbcan32.h"
#include "Systec/Include/UsbCanUp.h"
#include <QFile>
#include <QMutex>

class Systec : public QThread
{
    Q_OBJECT

    uint baudRate;                  //Индек скорости сети
    tUcanHandle UcanHandle;         //Указатель на устройство
    BYTE deviceNbr;                 //Номер устройства, нужен функции инициализации, так как может быть больше одного
    enum Operation {GETLOG, SCANNET, WRITE, READMEM};
    Operation op;                   //Определяет выполняемое действие
    int logType;                    //Сохраняет тип получаемого лога
    uint dataType;                  //Тип передаваемых данных
    uint eepromNo;                  //Номер переферийного устройства для прошивки
    uint readAdress, readLength;    //Адрес по которому читать и длина передаваемых или читаемых данных
    uint writeAdress;               //Адрес по которому писать
    QFile* soursefile;              //Файл, данные из которого передаются
    volatile bool read;             //Используется как флаг для продолжения или завершения цикла
    volatile bool running;          //Флаг продолжения потока
    QMutex mutex;                   //Мьютекс флага продолжения потока

    void error(int err);            //Выводит ошибку USB-CAN конвертера
    void getLog(int log_Type);      //Получает лог одним блоком
    int get100mess(QVector<tCanMsgStruct>* dataVect, uint dataLength); //Получает и анализирует на
                                    //ошибки блоки по 0х100 сообщений
    void getLogBy100Mess();         //Получения лога блоками по 0х100 сообщений
    void readMemory();              //Функция чтения по адресу, указанному в программе
    void writeTo();                     //Функция передачи данных в контроллер
    uint receiveCRC();              //Получает контрольную сумму
    uint receiveDataLength();       //Получает длину данных
    QList<uchar> sortVect(QVector<tCanMsgStruct>*);     //Возвращает отсортированный лист байтов данных
    QList<uint> sortGetKeys(QVector<tCanMsgStruct>*);   //Возращает отсортированный лист адресов данных
    void showCanError(uint errNbr);     //Выводит на экран ошибкти конвертера
    void readMess();                    //Функция сканирования сети ??? устаревшая
    bool Running();                     //Флаг продолжения потока
public:
    Systec(uint baud, QObject *parent);
    ~Systec();
    void run();
    void reInit(uint baud);             //Переподключает USB-CAN конвертер
    void setSpeed(uint speed);          //Устанавливает скорость передачи данных
    void setLogType(int log_Type);      //Сохраняет тип получаемого лога
    void writeGetReady(QFile* file, uint dType, uint eeprom);   //Передаёт данные, необходимые для передачи
    void writeSetAdress(uint adr);      //Сохраняет адрес, по которому нужно произвести запись
    void stopThread();                  //Устанавливает флаг окончания потока
    void clearBuffers();                //Передаёт в контроллер сообщене очистки буффером приёма и передачи данных
    void slotStartScanNet();            //Подгатавливает к сканированию сети
    void slotStopScanNet();             //Останавливает сканарование сети
    void slotReadMem(uint adr, uint length);    //Устанавливает адрес и длину получаемых данных

signals:
    void signalBoth(QString);       //выводит сообщени во все поля вывода
    void signalMainOut(QString);    //Выводит сообщение в поле вывода чтения
    void signalMainOut2(QString);   //Выводит сообщение в поле вывода записи
    void signalProgress(int);       //Устанавливает прогресс бар
    void signalLogEnd(uchar*, uint);//Возвращает полученный массив байт и его размер

};



#endif // SYSTEC_H
