#-------------------------------------------------
#
# Project created by QtCreator 2015-11-19T08:23:57
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CanTerm32
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    cancommunic.cpp \
    fileoperations.cpp \
    cancommunic2.cpp \
    cancommunicwrite.cpp \
    systec.cpp \
    crcdata.cpp

HEADERS  += mainwindow.h \
    cancommunic.h \
    modebox.h \
    crcdata.h \
    ui_mainwindow.h \
    fileoperations.h \
    systec.h \
    crcdata.h


FORMS    += mainwindow.ui

#LIBS += ./lib/microsoft/slcan.lib #D:/SL-USB-CAN/Projects/test/CanTerm32/lib/microsoft/slcan.lib
#LIBS += ./Systec/Lib/USBCAN32.lib #D:/SL-USB-CAN/Projects/test/CanTerm32/Systec/Lib/USBCAN32.lib
LIBS += D:/Work/CanTerm32/Systec/Lib/USBCAN32.lib
LIBS += D:/Work/CanTerm32/lib/microsoft/slcan.lib

DISTFILES += \
    slcan.dll

RESOURCES += \
    everything.qrc
#QMAKE_LFLAGS_WINDOWS = /SUBSYSTEM:WINDOWS,5.01
