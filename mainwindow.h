#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "cancommunic.h"
#include "systec.h"
#include "fileoperations.h"
#include "inc/slcan.h"
#include <QMap>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    Slavna* can;        //Ссылка на класс, реализующий функционал USB-CAN конвертера фирмы SLAVNA
    Systec* systec;     //Ссылка на класс, реализующий функционал USB-CAN конвертера фирмы SYSTEC
    enum Interface {SLAVNA, SYSTEC};
    Interface usbInterf;            //Определяе какой интерфейс является рабочим
signals:
   void signalSetOutput(QString);   //Выводит данные в поле вывода

private:
    Ui::MainWindow *ui;
    //QList<ushort> sortVect(QVector<SLCAN_MESSAGE>*);
    FileOperations* writeFile;      //Класс, сохраняющий прочитанные данные в файл в отдельном потоке
    uint CanBaudRate;               //Хранит индекс скорости сети
    QString lastOpenedFile;         //Сохраняет последний выбранный на запись или чтение файл,
                                    //что бы окно выбора файла открывалось в последней выбранной папке

public slots:
    void slotReciveDataArray(uchar *, uint);    //Получает полученный массив данных и отправляет на сохранение
    void slotStartLog1();           //Запускает чтение лога 1
    void slotStartLog2();           //Запускает чтение лога 2
    void setBusSpeed(int);          //Устанавливает скорость CAN сети
    void slotReconnect();           //Переподключает активное устройство
    void slotClearAndStop();        //Останавливает поток и очищает буфферы устройства
    void slotWriteToCan();          //Запускает передачу прошивки в контроллер
    void slotWriteToAdress();       //Запускает запись в контроллер файла по указанному адресу
    void slotClearOutput();         //Очищает поле вывода закладки чтения
    void slotClearOutput2();        //Очищает поле вывода закладки записи
    void slotStartReadMemory();     //Заускает чтение из памяти контроллера, адрес и длинна которой указывается
                                    //в поле ввода
    void slotActionSlavna();        //Выбирает в качестве активного устройства конвертер SLAVNA
    void slotActionSystec();        //Выбирает в качестве активного устройства конвертер SLAVNA
    void slotStartScanNet();        //Запускает сканирование сети
    void slotStopScanNet();         //Останавливает сканирование сети
};

#endif // MAINWINDOW_H
