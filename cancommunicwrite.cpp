#include "cancommunic.h"
#include "crcdata.h"
#include <iostream>
#include <QDebug>
#include <QTime>


void Slavna::writeGetReady(QFile* file, uint dType, uint eeprom)
{
    soursefile = file;
    op = Operation::WRITE;
    if(dType < 3) dataType = dType;
    else dataType = eeprom;
    running = true;
}

void Slavna::writeSetAdress(uint adr)
{
    writeAdress = adr;
}

void Slavna::writeTo()
{
    BYTE transStatus;
    QByteArray data;
    SLCAN_MESSAGE OutMsg;

    if(soursefile->open(QIODevice::ReadOnly))
    {
        data = soursefile->readAll();
        soursefile->close();
        delete soursefile;
        qDebug() << "Файл прочитан";
    }
    else
    {
        emit signalMessage2("Файл не открылся");
        return;
    }
    ulong dataLength = data.length();
    uchar* udata = new uchar[dataLength];
    uint maxval = static_cast<uint>(-1);
    if(dataLength > maxval)
    {
        emit signalMessage2("Размер файла слишком большой");
        qDebug() << maxval;
        return;
    }
    for(ulong i = 0; i < dataLength; i++)
    {
        udata[i] = data.at(i);
    }
    ushort crc_mask = 0xFFFF;
    ushort crc = CRC::get_crc_16(crc_mask, udata, dataLength);

    clear();
    BOOL b;

    QThread::msleep(150);
    if(!Running()) { return;}
    //Посылантся команда 2 для подготовки контроллера к приёму данных
    OutMsg.Info = SLCAN_MES_INFO_EXT;
    OutMsg.ID = SEND_BOX_ID;
    OutMsg.DataCount = 8;
    OutMsg.Data[0] = static_cast<uchar>((writeAdress >> 8) & 0xFF);
    OutMsg.Data[1] = static_cast<uchar>(writeAdress & 0xFF);
    OutMsg.Data[2] = 0x00;
    OutMsg.Data[3] = dataType;
    OutMsg.Data[4] = 0xE0;
    OutMsg.Data[5] = SEND_DATA_ADRESS;
    OutMsg.Data[6] = static_cast<uchar>((writeAdress >> 24)&0xFF);
    OutMsg.Data[7] = static_cast<uchar>((writeAdress >> 16)&0xFF);

    b = SlCan_DeviceWriteMessages(hDevice, &OutMsg, 1, &transStatus);
    if(b == 0)
    {
        emit signalMessage2("Сообщение инициализации отправки данных не отправлено");
        showErrorSlaviaMessage(transStatus);
        return;
    }

    QThread::msleep(150);
    //Отправляется длина файла
    OutMsg.Info = SLCAN_MES_INFO_EXT;
    OutMsg.ID = SEND_BOX_ID;
    OutMsg.DataCount = 8;
    OutMsg.Data[0] = static_cast<uchar>((dataLength >> 8)&0xFF);
    OutMsg.Data[1] = static_cast<uchar>(dataLength&0xFF);
    OutMsg.Data[2] = 0x00;
    OutMsg.Data[3] = dataType;
    OutMsg.Data[4] = 0xE0;
    OutMsg.Data[5] = SEND_DATA_LENGTH;
    OutMsg.Data[6] = static_cast<uchar>((dataLength >> 24)&0xFF);
    OutMsg.Data[7] = static_cast<uchar>((dataLength >> 16)&0xFF);

    b = SlCan_DeviceWriteMessages(hDevice, &OutMsg, 1, &transStatus);
    if(b == 0)
    {
        emit signalMessage2("Длина файла не отправлена");
        showErrorSlaviaMessage(transStatus);
        return;
    }
    QThread::msleep(150);
    //Отправляем сообщение c CRC
    OutMsg.Info = SLCAN_MES_INFO_EXT;
    OutMsg.ID = SEND_BOX_ID;
    OutMsg.DataCount = 8;
    OutMsg.Data[0] = static_cast<uchar>((crc >> 8)&0xFF);
    OutMsg.Data[1] = static_cast<uchar>(crc&0xFF);
    OutMsg.Data[2] = 0x00;
    OutMsg.Data[3] = 0x01;
    OutMsg.Data[4] = 0xE0;
    OutMsg.Data[5] = SEND_CRC;
    OutMsg.Data[6] = 0x00;
    OutMsg.Data[7] = 0x00;

    b = SlCan_DeviceWriteMessages(hDevice, &OutMsg, 1, &transStatus);
    if(b == 0)
    {
        emit signalMessage2("Код CRC не отправлен");
        showErrorSlaviaMessage(transStatus);
        return;
    }
    QThread::msleep(150);
    if(dataType == 0) emit signalMessage2("Запись в контроллер началась");
    else if(dataType == 1) emit signalMessage2("Передача прошивки контроллера началась");
    else if(dataType == 2) emit signalMessage2("Передача прошивки Xilinx началась");
    else emit signalMessage2("Передача прошивки EEPROM началась");
    //Передача данных блоками по 100 штук
    SLCAN_MESSAGE OutArr[10];
    uint i = 0;
    uint sent = 0;
    uint procent = dataLength / 100;
    procent = procent > 0 ? procent : 1;
    while(sent < dataLength)
    {
        if(!Running()) { return;}
        OutArr[i].Info = SLCAN_MES_INFO_EXT;
        OutArr[i].ID = SEND_BOX_ID;
        OutArr[i].DataCount = 8;
        OutArr[i].Data[6] = (sent >> 24) & 0xFF;
        OutArr[i].Data[7] = (sent >> 16) & 0xFF;
        OutArr[i].Data[0] = (sent >> 8) & 0xFF;
        OutArr[i].Data[1] = sent & 0xFF;
        OutArr[i].Data[2] = udata[sent++];
        OutArr[i].Data[3] = udata[sent++];
        OutArr[i].Data[4] = 0xE0;
        OutArr[i].Data[5] = SEND_DATA_WORD;
        if(sent % procent == 0) { emit signalProgress(sent/procent);}
        b = SlCan_DeviceWriteMessages(hDevice, OutArr, 1, &transStatus);
        if(b == 0)
        {
            emit signalMessage2("Ошибка при передаче блока данных");
            showErrorSlaviaMessage(transStatus);
            return;
        }
        QThread::usleep(1000);
    }
    qDebug() << "После отправки данных. Отправлено " << sent << "байт";
    delete[] udata;

    OutMsg.Data[0] = 0x00;
    OutMsg.Data[1] = 0x00;
    OutMsg.Data[2] = 0x00;
    OutMsg.Data[3] = dataType;
    OutMsg.Data[4] = 0xE0;
    OutMsg.Data[5] = SEND_ARRAY_END;
    OutMsg.Data[6] = 0x00;
    OutMsg.Data[7] = dataType;

    b = SlCan_DeviceWriteMessages(hDevice, &OutMsg, 1, &transStatus);
    if(b == 0)
    {
        emit signalMessage2("Сообщение окончания передачи не отправлено");
        showErrorSlaviaMessage(transStatus);
        return;
    }
qDebug() << "Сообщение окнчания передачи отправлено";
    SLCAN_MESSAGE InMsg[10];
    bool sucsess = false;
    DWORD readStatus;
    QTime timer;
    timer.start();
    while(true)
    {
        if(!Running()) { return;}
        b = SlCan_DeviceReadMessages(hDevice, 50, InMsg, 1, &readStatus);
        if(b == 0)
        {
            emit signalMessage2("ERROR Ответ о получении от контроллера не получен");
            showErrorSlaviaMessage(readStatus);
            return;
        }
        if(InMsg[0].ID == RECIEVE_BOX_ID)
        {
            uint command = ((InMsg[0].Data[4] & 0x1F) << 8) + (InMsg[0].Data[5] & 0xFF);
            if (command == ERROR_MESSAGE) {
                uint errorNmbr = ((InMsg[0].Data[6] & 0xFF) << 8) + (InMsg[0].Data[7] & 0xFF);
                if(errorNmbr == 1)
                {
                    sucsess = true;
                    emit signalMessage2("Данные в контроллер передались без ошибок");
                }
                else
                {
                    showCanError(errorNmbr);
                    return;
                }
            }
        }
        if(timer.elapsed() > 5000)
        {
            qDebug() << "Ждём ответа";
            timer.restart();
        }
    }
    if(sucsess && dataType)
    {
        //Посылается команда начала прошивки
        OutMsg.Data[0] = 0x00;
        OutMsg.Data[1] = 0x00;
        OutMsg.Data[2] = 0x00;
        OutMsg.Data[3] = dataType;
        OutMsg.Data[4] = 0xE0;
        OutMsg.Data[5] = START_WRITE;
        OutMsg.Data[6] = 0x00;
        OutMsg.Data[7] = 0x00;

        b = SlCan_DeviceWriteMessages(hDevice, &OutMsg, 1, &transStatus);
        if(b == 0)
        {
            emit signalMessage2("Сообщение начала прошивки во Flesh не отправлено");
            showErrorSlaviaMessage(transStatus);
            return;
        }
        emit signalMessage2("Прошивка началась");

        while(true)
        {
            if(!Running()) { return;}
            b = SlCan_DeviceReadMessages(hDevice, 50, InMsg, 1, &readStatus);
            if(b == 0)
            {
                emit signalMessage2("Ответ о получении от контроллера не получен");
                showErrorSlaviaMessage(readStatus);
                return;
            }
            if(InMsg[0].ID == RECIEVE_BOX_ID)
            {
                uint command = ((InMsg[0].Data[4] & 0x1F) << 8) + (InMsg[0].Data[5] & 0xFF);
                if (command == ERROR_MESSAGE)
                {
                    uint errorNmbr = ((InMsg[0].Data[2] & 0xFF) << 8) + (InMsg[0].Data[3] & 0xFF);
                    if(errorNmbr == 1)
                    {
                        sucsess = true;
                        emit signalMessage2("Запись во Flash прошла успешно");
                    }
                    else
                    {
                        showCanError(errorNmbr);
                    }
                }
            }
        }
    }
}
